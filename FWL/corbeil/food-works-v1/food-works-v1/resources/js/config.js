jQuery( document ).ready(function( $ ){

	$( '.site-header' ).affix({
		offset : {
			top : function(){
				return ( $( '.hero' ).outerHeight() - $( '.site-header' ).outerHeight() );
			}
		}
	});

	// iframe fancyboxes
	$( '.fancybox-iframe-trigger' ).fancybox({
		type : 'iframe',
		helpers: {
			overlay: {
				locked: false
			}
		},
		afterShow: function () {
			$( this.content ).attr( 'tabindex' , 0 ).focus()
		}
	});

});
