const markdown = require( 'helper-markdown' );

module.exports = {
	even_odd : function( index, options ){
		if( ( index % 2 ) == 0 ) {
			return options.fn( this );
		} else {
			return options.inverse( this );
		}
	},
	markdown : markdown()
}
