var gulp = require('gulp'),
	sass = require( 'gulp-sass'),
	notify = require( 'gulp-notify' ),
	concat = require( 'gulp-concat' ),
	uglify = require( 'gulp-uglify' ),
	imagemin = require( 'gulp-imagemin' ),
	autoprefixer = require( 'gulp-autoprefixer' ),
	browserSync = require( 'browser-sync' ).create(),
	nodemon = require( 'gulp-nodemon' ),
	config = require( './config.js' ),
	resources = './resources',
	modules = './node_modules',
	dist = './public';


// images
gulp.task( 'images', function(){

	gulp.src( modules + '/fancybox/dist/img/**.*' )
	.pipe(
		imagemin({
			progressive: true,
			svgoPlugins: [{ removeViewBox: false }]
		})
	)
	.pipe( gulp.dest( dist + '/images' ) )
	.pipe( browserSync.stream() );

	gulp.src( resources + '/images/*.**' )
	.pipe( gulp.dest( dist + '/images' ) )
	.pipe( browserSync.stream() );

});

// fonts
gulp.task( 'fonts', function(){

	gulp.src([
		resources + '/fonts/*.**',
		modules + '/bootstrap-sass/assets/fonts/bootstrap/*.**',
	])
	.pipe( gulp.dest( dist + '/fonts' ) )
	.pipe( browserSync.stream() );

});

// scss
gulp.task( 'scss', function(){

	gulp.src( resources + '/scss/*.scss' )
	.pipe(
		sass({
			outputStyle: 'compressed'
		}).on( 'error', notify.onError(function( error ) {
			return 'Error: ' + error.message;
		}))
	)
	.pipe(autoprefixer({
		browsers: [ 'last 2 versions', '> 2%']
	}))
	.pipe( gulp.dest( dist + '/css' ) )
	.pipe( browserSync.stream() );

});

// js
gulp.task( 'js', function(){

	gulp.src([
		modules + '/jquery/dist/jquery.js',
		modules + '/bootstrap-sass/assets/javascripts/bootstrap.js',
		modules + '/fancybox/dist/js/jquery.fancybox.pack.js',
		resources + '/js/config.js'
	])
	.pipe( concat( 'scripts.js' ))
	.pipe( uglify({ mangle : false }) )
	.pipe( gulp.dest( dist + '/js' ) );

});

gulp.task( 'watch', function(){

	browserSync.init(
		null,
		{
			proxy : 'http://localhost:5000',
			port : 3000,
		}
	);

	nodemon({
		script : 'index.js'
	}).on('quit', function(){
		process.exit();
	});

	gulp.watch( resources + '/scss/*.scss', [ 'scss' ] );
	gulp.watch( resources + '/js/*.js', [ 'js' ] ).on( 'change', browserSync.reload )
	gulp.watch( './views/**/**.*' ).on( 'change', browserSync.reload );

});

gulp.task( 'build', [ 'images', 'fonts', 'scss', 'js' ] );
gulp.task( 'start', [ 'build', 'watch' ] );
