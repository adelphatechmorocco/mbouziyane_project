const express = require( 'express' );
const handlebars  = require( 'express-handlebars' );
const bodyParser = require( 'body-parser' );
const config = require( './config.js' );
const helpers = require( './helpers.js' );
const app = express();

app.use( express.static('public') );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended : false }) );
app.engine(
	'handlebars',
	handlebars({
		extname : 'handlebars',
		defaultLayout : 'main',
		layoutsDir : __dirname + '/views/layouts/',
		helpers : helpers
	})
);

app.set( 'view engine', 'handlebars' );

app.use( '/', require( './routes/pages.js' ) );

app.listen( config.port, function(){
	console.log( 'Example app listening on port:' + config.port )
});
