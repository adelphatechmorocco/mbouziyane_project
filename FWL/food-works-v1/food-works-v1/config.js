const config = require( 'dotenv' ).config();
const contentful = require( 'contentful' );

module.exports = {
	node_env : process.env.NODE_ENV,
	port : process.env.PORT || 5000,
	contentful : contentful.createClient({
		space : process.env.CONTENTFUL_SPACE_ID,
		accessToken : process.env.CONTENTFUL_ACCESS_TOKEN,
		resolveLinks : true
	}),
	typeform : process.env.TYPEFORM,
	menu : {
		home : '159DFP0xqUMqkaiewc2m8U',
		partners : '1shehkK9remuWAk0eWMIq8',
		about : '5xEmOSfKlUusSqckoIEOey',
		how : '1CrqBrRNIg6eEaGem8MmyI'
	},
	settings : {
		contact : '2o50qvNjmcA6YQ0YKUgUEs'
	}
}
