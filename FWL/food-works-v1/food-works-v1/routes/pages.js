var express = require( 'express' ),
	config = require( '../config.js' ),
	functions = require( '../functions.js' ),
	router = express.Router();

router.get( '/', function( req, res ){

	var promises = [],
		data = {
			title : 'Food Works | Home',
			typeform : config.typeform,
			settings : {}
		};

	promises.push(
		Object.keys( config.settings ).forEach( key => {
			config.contentful.getEntries({ 'sys.id' : config.settings[ key ] }).then( instance => {
				data.settings[ key ] = instance.items[ 0 ].fields;
			})
		})
	);

	promises.push(
		config.contentful.getEntries({ 'sys.id' : config.menu.home }).then( instance => {

			var page = instance.items[ 0 ].fields;
				page.sticky = {};

			if( page.features.length ){

				var features = page.features;
				delete page.features;
				page.sticky.feature = features[ 0 ];
				page.features = features.slice( 1 );

			}

			if( page.advertisements.length ){

				var advertisements = page.advertisements;
				delete page.advertisements;
				page.sticky.advertisement = advertisements[ 0 ];
				page.advertisements = advertisements.slice( 1 );

			}

			data.page = page;

		})
	);

	Promise.all( promises ).then( completed => {

		return res.render(
			'home',
			data
		);

	});

});

router.get( '/partners', function( req, res ){

	var promises = [],
		data = {
			title : 'Food Works | Partners',
			typeform : config.typeform,
			settings : {}
		};

	promises.push(
		Object.keys( config.settings ).forEach( key => {
			config.contentful.getEntries({ 'sys.id' : config.settings[ key ] }).then( instance => {
				data.settings[ key ] = instance.items[ 0 ].fields;
			})
		})
	);

	promises.push(
		config.contentful.getEntries({ 'sys.id' : config.menu.partners }).then( instance => {

			var page = instance.items[ 0 ].fields;

			if( page.partners.length ){

				var partners = functions.array_chunk( page.partners, 3 );
				delete page.partners;
				page.partners = partners;

			}

			data.page = page;

		})
	);

	Promise.all( promises ).then( completed => {

		return res.render(
			'partners',
			data
		);

	});

});

router.get( '/how-it-works', function( req, res ){

	var promises = [],
		data = {
			title : 'Food Works | How It Works',
			typeform : config.typeform,
			settings : {}
		};

	promises.push(
		Object.keys( config.settings ).forEach( key => {
			config.contentful.getEntries({ 'sys.id' : config.settings[ key ] }).then( instance => {
				data.settings[ key ] = instance.items[ 0 ].fields;
			})
		})
	);

	promises.push(
		config.contentful.getEntries({ 'sys.id' : config.menu.how }).then(function( instance ){
			data.page = instance.items[ 0 ].fields;
		})
	);

	Promise.all( promises ).then( completed => {

		return res.render(
			'page',
			data
		);

	});

});

router.get( '/about', function( req, res ){

	var promises = [],
		data = {
			title : 'Food Works | About',
			typeform : config.typeform,
			settings : {}
		};

	promises.push(
		Object.keys( config.settings ).forEach( key => {
			config.contentful.getEntries({ 'sys.id' : config.settings[ key ] }).then( instance => {
				data.settings[ key ] = instance.items[ 0 ].fields;
			})
		})
	);

	promises.push(
		config.contentful.getEntries({ 'sys.id' : config.menu.about }).then(function( instance ){
			data.page = instance.items[ 0 ].fields;
		})
	);

	Promise.all( promises ).then( completed => {

		return res.render(
			'page',
			data
		);

	});

});

module.exports = router;
