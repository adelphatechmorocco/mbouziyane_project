<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>EMINES - School of Industrial Management</title><!-- Bootstrap style sheet -->
	<link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/custom.css" id="style" rel="stylesheet">
	<link href="css/color-blue.css" id="colors" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/switcher.css" rel="stylesheet">
	<link href="css/layout.css" rel="stylesheet">
	<link rel="stylesheet" href="css/responsive.css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="central-casa">
	<!-- main wrapper of the page -->
	<div id="wrapper">
		<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
			</div>
		</div><?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img alt="image" height="157" class="insaban" src="images/ce.png" width="1920">
			<div class="banner-text">
				<h1>EMINES - School of Industrial Management</h1>
				<h3></h3>
			</div>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>EMINES - SCHOOL OF INDUSTRIAL MANAGEMENT</li>
			</ul>
		</div>
		<section class="page-section white short" style="padding: 13px 0;">
			<div class="container">
				<div class="row tool-tip yellow">
					<div class="col-sm-4"><img style="width: 340px" src="images/EMINES.JPG"></div>
					<div class="col-sm-8">
						<p class="text-justify">Résolument orientée vers le monde des entreprises, l’EMINES-School of Industrial Management, qui a ouvert ses portes en septembre 2013, en tant que première « brique » de l’Université Mohammed VI Polytechnique à Ben Guérir, a pour vocation de développer des programmes de formation et de recherche capables de répondre aux principaux défis et enjeux posés par l’industrialisation durable du continent africain.

</p>
						<p>A cette fin, l’EMINES - School of Industrial Management forme des ingénieurs managers à Bac +5, des ingénieurs spécialisés à Bac +6, et des ingénieurs docteurs à Bac +9 maitrisant les savoirs et savoir-faire scientifiques et techniques de base, mais aussi capables d’appréhender à la fois le caractère global des entreprises et les différentes composantes (économique, technologique, sociale, environnementale, entrepreneurial) de leur fonction.
Pour cela, elle s’appuie sur un partenariat original avec MINES ParisTech et son réseau d'enseignants chercheurs, et développe une pédagogie, dans tous ses programmes de formation, basée sur le learning by doing.</p>
					</div>
				</div>
			</div>
		</section>
		<div class="page-section short">
			<div class="container">
				<div class="row">
					<section class="services-tab short col-md-12">
						<div>
							<div class="tab_2 second yellow">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active" role="presentation">
										<a aria-controls="home3" data-toggle="tab" href="#home3" role="tab"><img alt="" class="center-block" src="images/business-people-meeting.png" style="display: block">Formation Proposée</a>
									</li>
									<li role="presentation">
										<a aria-controls="home" data-toggle="tab" href="#home9" role="tab"><img alt="" class="center-block" src="images/diploma.png" style="display: block">Reconnaissance du diplôme</a>
									</li>
									<li role="presentation">
										<a aria-controls="profile" data-toggle="tab" href="#profile" role="tab"><img alt="" class="center-block" src="images/icon-suit.png" style="display: block"> Débouchés professionnels</a>
									</li>
									<li role="presentation">
										<a aria-controls="frais" data-toggle="tab" href="#frais" role="tab"><img class="center-block" src="images/graduate-showing-his-diploma.png" style="display: block">Intégrer notre établissement</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="#" id="linkmsg"><img alt="" class="center-block" src="images/email.png" style="display: block"> Plus d'informations</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="http://www.emines-ingenieur.org/" target="_blank" id="linkmsg"><img alt="" class="center-block" src="images/www.png" style="display: block"> Site de l’établissement </a>
									</li>
								</ul><!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane fade in active" id="home3" role="tabpanel">
										<p>L’offre de formation de l’EMINES - School of Industrial Management est constituée de :</p>

									<ul>
										<li>Un Cycle Préparatoire Intégré, d’une durée de deux ans, accessible sur concours après le baccalauréat.</li>
										<li>Un Cycle Ingénieur, d’une durée de trois ans, accessible sur concours après une classe préparatoire scientifique, une licence scientifique, ou par la voie de son Cycle Préparatoire Intégré.</li>
										<li>Un Mastère Spécialisé®, co-diplômant avec l’Ecole des Mines de Paris (MINES ParisTech), d’une durée d’un an, accessible sur concours après un diplôme BAC+5 ou un BAC+4 avec expériences professionnelles.</li>
										<li>Un PhD Industriel, d’une durée de quatre ans, accessible sur concours après un diplôme BAC+5. </li>
										<li>Une offre d’executive education à destination des entreprises, construite à la fois à partir de modules de formation ouverts à la formation continue, et à partir de programmes développés sur mesure.</li>
									</ul>


										</p>

											<p>Plus d’infos sur <a href="http://www.emines-ingenieur.org/" target="_blank"> www.emines-ingenieur.org </a></p>
									</div>
									<div class="tab-pane fade" id="home9" role="tabpanel">
										<p> Au plan national, l'EMINES – School of Industrial Management et ses formations bénéficient des éléments de reconnaissance suivants :
</p>
										<p>Autorisation de l’Ecole N° 404/13 du 28/11/2014.</br>
										<ul>
											<li>Autorisation de la filière Management Industriel (Cycle Préparatoire Intégré et du Cycle Ingénieur) n° 02/1528 du 03/06/2015.</li>
											<li>Accréditation de la filière Management Industriel (Cycle Préparatoire Intégré et du Cycle Ingénieur) n° 02/1626 du 23/06/2015.</li>
											<li>L’Université Mohammed VI Polytechnique est déclarée bénéficier d’un Partenariat Public Public (PPP) par arrêté du MESRSFC de mai 2015, qui autorise le principe de Reconnaissance par l’Etat.</li>
											<li>Demande de Reconnaissance par l’Etat de l’Université Mohammed VI Polytechnique (qui conduira à l’Equivalence des Diplômes aux Diplômes d’Etat) déposée le 31 mars 2016.</li>
										</ul>

</p>


									<p>Au plan international, l'EMINES – School of Industrial Management et ses formations bénéficient des éléments de reconnaissance suivants : </p>
<ul>
	<li>Accréditation du Mastère Spécialisé® MILEO, diplôme à Bac+6, co-diplômant avec l’Ecole des Mines de Paris (MINES ParisTech), la Conférence des Grandes Ecoles en France par la session de juin 2014.</li>
	<li>La demande d’Accréditation de la filière Management Industriel (Cycle Préparatoire Intégré et du Cycle Ingénieur) par la Commission des Titres Ingénieurs française (CTI) sera formulée en 2017, quand les premiers étudiants auront été diplômés.</li>

</ul>



</p>


									</div>
									<div class="tab-pane fade" id="profile" role="tabpanel">
										<p>La formation d’ingénieurs en Management Industriel cherche à préparer les jeunes diplômés à exercer des métiers de « managers industriels » dans les entreprises industrielles du Maroc. Les options de spécialisation ont été ainsi choisies en fonction des besoins recensés dans ces mêmes entreprises, et plus précisément :</p>
										<p>Supply Chain Management – pour tout type d’entreprises (automobile, construction aéronautique, agro-alimentaire, textile, grande distribution, prestataires logistiques, conseil) souhaitant renforcer ses capacités dans le domaine de la supply chain (achat/approvisionnement, manufacturing, distribution, aide à la décision-système d’information).</br>
										Mines et BTP – pour les entreprises extractives (OCP, Managem, Lafarge Maroc …).</br>
										Energie renouvelable – pour toutes les nouvelles entreprises développement alternatives.</p>
									</div>
									<div class="tab-pane fade" id="frais" role="tabpanel">
										<div class="col-xs-12">
											<strong>Niveau d’admission</strong>
											<p>Admission en cycle préparatoire intégré :  Le cycle préparatoire intégré est ouvert aux titulaires d’un Baccalauréat scientifique désireux de réaliser une formation conduisant à un diplôme d’Ingénieur en Management Industriel.</br>
											Admission au cycle d’ingénieur : en BAC + 2, BAC+3 </br>
											Admission en Mastère spécialisé</p>
											<strong>Procédure de candidature</strong>
											<p>Candidature sur dossier et concours. Les candidatures doivent être déposées en ligne sur le site de EMINES.</p>
											<strong>Frais scolaire</strong>
											<p>En cycle préparatoire intégré et cycle ingénieur :  75 000 Dhs / an (frais à taux maximal)</br>
											En Mastère Spécialisé MILEO :  103 000 Dhs / an avec restauration et hébergement compris (frais à taux maximal).</p>
											<strong>Aide financière</strong>
											<p>Les élèves méritants pourront bénéficier de bourses d’excellence, couvrant 25%, 50%, 75% ou 100% des frais de scolarité, attribuée en fonction des résultats au concours d'entrée à l'école pour l’ensemble de la scolarité.   </br>
											Le cas d’élèves en difficulté financière sera examiné par la Commission des bourses qui pourra accorder une bourse sociale, pouvant couvrir 25%, 50%, 75% ou 100% des frais de scolarité, et tout ou partie des frais de logement, de nourriture.

											</p>
											<p>Les bourses d’excellence et les bourses sociales sont cumulables.</p>

											<p><p>Plus d’infos sur <a href="https://www.emines-ingenieur.org/formation/frais-de-scolarite" target="_blank"> www.emines-ingenieur.org/formation/frais-de-scolarite </a></p></p>
											<strong>Logement</strong>
											<p>1 000 Dhs par mois pour une chambre individuelle dans un appartement de 4 chambres, dans la résidence du campus.
Les appartements (groupes de chambres individuelles) sont équipés d’une cuisine, et permettent donc aux étudiants qui le désirent d'être autonomes pour tout ou partie de leurs repas. Les repas peuvent également être pris dans la cafétéria de l'école ou dans le restaurant de l’Université.</p>
										</div>
										<div class="row tool-tip yellow">
											<div class="col-xs-12">
												<div class="row">
													<div class="col-sm-12 no-padding-left"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="page-section white" style="padding-top: 50px; padding-bottom: 50px">
			<section class="contact-us yellow">
				<div class="container">
					<div class="owl-theme cetraleslide" id="owl-demo">
						<div class="item"><img alt="Owl Image" src="images/em1.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/em2.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/CampusCentrale3.jpg"></div>
							<div class="item"><img alt="Owl Image" src="images/emine5.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/emines6.jpg"></div>

					</div>
				</div>
			</section>
		</div>
		<div class="page-section short" id="s6" style="padding: 12px 0;">
			<section class="contact-us yellow contact-us2">
				<div class="container">
					<h1 class="short">NOUS CONTACTER</h1>
					<div class="row">
						<div class="two-columns">
							<div class="col-sm-4 col-xs-12 padding">
								<div class="field-column yellow border contactb">
									<strong class="real-estate">ADRESSE</strong>
									<p>LOT 660 – HAY MOULAY RACHID, 43150 BEN GUERIR MAROC</p>
								</div>
							</div>
							<div class="col-sm-8 col-xs-12 padding">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d108014.18027939928!2d-8.00559797494837!3d32.2192327902137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xdaf7a3f90f200f3%3A0x45ee66ce93fdf2cb!2sEmines+%E2%80%93+School+Of+Industrial+Management!5e0!3m2!1sen!2s!4v1505288231404" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="page-section short2">
			<section class="contact-us yellow">
				<div class="container">
					<div class="row">
						<div class="two-columns">
							<h1 class="short">Vous avez besoin de plus d'informations ?</h1>
							<div class="col-sm-6 col-xs-12 padding ginfo">
								<span class="titleinfo"></span> <span class="tel cinfo"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:+212522493500">+212 (0) 5 22 49 35 00</a></span> <span class="mail cinfo"></span> <span class="website cinfo"><i aria-hidden="true" class="fa fa-globe"></i> <a href="http://www.emines-ingenieur.org/" target="_blank">www.emines-ingenieur.org/</a></span>
								<ul id="social-media-etab">
									<li>
										<a href="https://fr-fr.facebook.com/EMINES.UM6P" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a href="https://www.linkedin.com/company/emines-um6p" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a>
									</li>
								</ul>
							</div>
							<div class="col-sm-6 col-xs-12 padding">
                            <form action="mail.php" class="contact-form" method="POST">
                                <input placeholder="Nom & Prénom*" type="text" name="full_name"> <input placeholder="Email*" type="email" name="email"> <input placeholder="Sujet" type="text" name="sujet">
                                <textarea placeholder="Message" name="message"></textarea>
                                <div class="holder yellow">
                                    <button type="submit">Envoyer</button> <span><em>*</em>Champs obligatoires</span>
                                </div>
                                <input type="hidden" name="to" value="contact@emines-ingenieur.org" />
                            </form>
                        </div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div><?php include 'includes/footer.php'?>
	<script src="js/jquery.min.js">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->

	<script src="js/owl.carousel.js">
	</script>
	<script type="text/javascript">
	              $(document).ready(function() {

	                  $("#owl-demo").owlCarousel({
	                        autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

	                      responsive: {
	                          0: {
	                              items: 1,
	                              nav: false
	                          },
	                          600: {
	                              items: 2,
	                              nav: false
	                          },
	                          1000: {
	                              items: 3,
	                              nav: false,

	                          }
	                      }

	                  });

	              });
	</script>
	<script src="js/bootstrap.min.js">
	</script>
	<script src="js/switcher.js">
	</script>
	<script src="js/custom.js">
	</script>
</body>
</html>
