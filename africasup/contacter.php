<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- title of the page -->
<title>Nous contacter</title>


<!-- Bootstrap style sheet -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- css style sheet -->
<link rel="stylesheet" href="css/style.css">
<link id="style" rel="stylesheet" href="css/custom.css">
<link id="colors" rel="stylesheet" href="css/color-blue.css">
<link rel="stylesheet" href="css/switcher.css">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/responsive.css">
<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"> 

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<!-- main wrapper of the page -->
<div id="wrapper"> 
	<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png" 
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png" 
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
			</div>
		</div>

	<?php include 'includes/header.php'?>
		
	<div class="banner-2 yellow">
		<img src="images/ce.png" width="1920" height="157" alt="image">
		<div class="banner-text">
			<h2>Nous contacter</h2>

		</div>	</div>
		<div class="container">
			<ul class="breadcrumb ">
				<li><a href="http://africasup.org/">Accueil</a></li>
				<li>Nous contacter</li>
			</ul>
		</div>


	<div class="page-section white">
		<div class="container">
			<div class="row">
				<section class="contactus-form yellow">
					<form action="#">
						<div class="form-row">
							<div class="left-row">
								<input type="text"  placeholder="Nom & Prénom *">
							</div>
							<div class="right-row">
								<input type="email" placeholder="Email *">
							</div>
						</div>
						<div class="form-row">
							<input type="text" placeholder="Sujet">
						</div>
						<textarea placeholder="Message"></textarea>
						<button type="submit">Envoyer</button>
						<em><i>*</i> Champs Obligatoires</em>
					</form>
				</section>
			</div>
		</div>
	</div>

	<div class="page-section">
		<section style="" class="map">
			<iframe src="https://www.google.com/maps/d/embed?mid=1_1pUF3KXksR-os9sdB12qW91Bso" width="640" height="480" frameborder="0" ></iframe>
		</section>
	</div>



	<?php include 'includes/footer.php'?>
			
		</div>


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/switcher.js"></script>
	<script src="js/custom.js"></script>
	
	</body>
</html>
