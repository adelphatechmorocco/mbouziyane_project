<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>ESSEC Afrique-Atlantique</title><!-- Bootstrap style sheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/custom.css" id="style" rel="stylesheet">
    <link href="css/color-blue.css" id="colors" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/switcher.css" rel="stylesheet">
    <link href="css/layout.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="central-casa">
<!-- main wrapper of the page -->
<div id="wrapper">
    <div class="top-bar4">
        <div class="container">
             <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
        </div>
    </div>
    <?php include 'includes/header.php'?>
    <div class="banner-2 yellow">
        <img alt="image" height="157" class="insaban" src="images/ce.png" width="1920">
        <div class="banner-text">
            <h1>ESSEC Business School</h1>
            <h3>L’esprit pionnier</h3>

        </div>
    </div>
    <div class="container">
        <ul class="breadcrumb ">
            <li>
                <a href="http://africasup.org/">Accueil</a>
            </li>
            <li>ESSEC BUSINESS SCHOOL</li>
        </ul>
    </div>
    <section class="page-section white short">
        <div class="container">
            <div class="row tool-tip yellow">
                <div class="col-sm-4"><img class="imagees" src="images/ESSEC.png" alt="Owl Image"></div>
                <div class="col-sm-8">
                    <p class="text-justify">Fondée en 1907, l’ESSEC Business School est un acteur majeur de l’enseignement de la gestion sur la scène mondiale. Avec 4 campus dans le monde (Cergy, Paris, Rabat et Singapour) et plus de 182 universités partenaires dans 45 pays, l’ESSEC vous offre une large gamme programmes en formation initiale et continue tournée vers l’internationale. <br/>
L’ESSEC propose des enseignements en anglais et en français, et vous offre la possibilité d’étudier sur nos différents campus.<br/>
Avec un corps professoral composé de 158 professeurs permanents reconnus pour la qualité et l’influence de leurs recherches, l’ESSEC perpétue une tradition d’excellence académique et cultive un esprit d’ouverture au service des activités économiques, sociales et de l’innovation.<br/>
Enfin, avec 47 000 diplômés, répartis sur les 5 continents, vous bénéficiez d’un réseau puissant, influent et solidaire dans tous les secteurs d’activité. Choisir l’ESSEC Business School c’est se donner tous les moyens professionnels et personnels pour réussir son entrée dans la vie active. </p>

                </div>

            </div>
        </div>
    </section>
    <div class="page-section  short">
        <div class="container">
            <div class="row">
                <section class="services-tab short col-md-12">
                    <div>
                        <div class="tab_2 second yellow">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active" role="presentation">
                                    <a aria-controls="home3" data-toggle="tab" href="#home3" role="tab"><img src="images/business-people-meeting.png" class="center-block" alt="" style="display: block">Formation Proposée</a>
                                </li>
                                <li  role="presentation">
                                    <a aria-controls="home" data-toggle="tab" href="#home9" role="tab"><img src="images/diploma.png" class="center-block" alt="" style="display: block">Reconnaissance du diplôme</a>
                                </li>


                                <li role="presentation">
                                    <a aria-controls="profile" data-toggle="tab" href="#profile" role="tab"><img src="images/icon-suit.png" class="center-block" alt=""  style="display: block"> Débouchés professionnels</a>
                                </li>
                                <li role="presentation">
                                    <a aria-controls="frais" data-toggle="tab" href="#frais" role="tab"><img src="images/graduate-showing-his-diploma.png" class="center-block"  style="display: block"></i>Intégrer notre établissement
                                    </a>
                                </li>
                                <li class="tablink" role="presentation">
                                        <a data-value="s6" href="#" id="linkmsg"><img alt="" class="center-block" src="images/email.png" style="display: block"> Plus d'informations</a>
                                    </li>
                                    <li class="tablink" role="presentation">
                                        <a target="_blank" href="http://www.essec.edu/fr/" id="linkmsg"><img alt="" class="center-block"  src="images/www.png" style="display: block"> Site de l’établissement</a>
                                    </li>
                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="home3" role="tabpanel">

                                    <p>ESSEC Global BBA (Bachelor in Business Administration) : BBA numéro 1 en France.</p>
                                        <p> Mais aussi
                                        -  en formation initiale : MSc in Management
                                        - Grande Ecole, Global MBA, Masters, Mastères spécialisés.
                                       </p>
                                </div>

                                <div class="tab-pane fade" id="home9" role="tabpanel">

                                    <p> Reconnu par l’Etat (France), visé Bac +4.</p>
                                    <p> Autorisé au Maroc.</p>
                                    <p> 8 doubles-diplômes possibles.</p>
                                </div>


                                <div class="tab-pane fade" id="profile" role="tabpanel">

                                    <p>55% intègrent un Master dans un prestigieux établissement partenaires comme la
                                        London Business School, University of Sydney, Standford University…
                                      </p>
                                    <p>  45 % des étudiants créent leur entreprise ou intègrent le monde du travail en 4 mois
                                        en moyenne au poste de country manager, associate director, directeur marketing,
                                        directeur financier, project manager etc.</p>
                                </div>
                                <div class="tab-pane fade" id="frais" role="tabpanel">

                                    <div class="col-xs-12">
                                        <strong>Niveau d’admission</strong>
                                        <p>Post-bac</p>


                                        <strong>Procédure de candidature</strong></br>

                                        <ul class="rounded">
                                        <li>

                                                Deux voies d’admission sont possibles :

                                        </li>
                                       </ul>

                                    <ul class="dashed">
                                                        <li>sur concours Sesame pour les étudiants en lycée Français en France ou à
                                            l’étranger.
                                                        </li>
                                                        <li>sur dossier pour les étudiants dans un lycée non Français.
                                                        </li>
                                                    </ul>

                                        <strong>Frais scolaires  </strong>
                                        <ul class="rounded">
                                        <li>

                                        Frais de scolarité : 12 000 euros/an*

                                        </li>
                                       </ul>


                                        <strong> Aide financière </strong>


                                        <ul class="rounded">
                                        <li>Pionnier : 2000 euros pour tout étudiant débutant son parcours au Campus
                                            ESSEC Afrique-Atlantique. </li>
                                        <li>Excellence : jusqu’à 2000 euros. </li>
                                        <li>Mérite : jusqu’à 2000 euros. </li>
                                        <li>Bourses possibles sur le campus ESSEC Afrique-Atlantique</li>
                                        <li>Plus d’infos sur <a href="www.essec.edu/fr">www.essec.edu/fr</a></li>
                                       </ul>
                                        <strong>Logement  </strong>
                                        <p>Les élèves de l’ESSEC Afrique-Atlantique sont logés en résidence universitaire sur le
                                            campus ESSEC (chambre simple ou double).
                                            Activités à proximité immédiate du campus : surf, golf, football, équitation etc.</p>
 </br></br>
 <p>*montants susceptibles de modifications</p>
                                    </div>
                                    <div class="row tool-tip yellow">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-sm-12 no-padding-left">


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="page-section white" style="padding-top: 50px; padding-bottom: 50px">
        <section class="contact-us yellow">
            <div class="container">
                <div class="owl-theme cetraleslide" id="owl-demo">
                    <div class="item"><img alt="Owl Image" src="essec/e2.jpg"></div>
                    <div class="item"><img alt="Owl Image" src="essec/e1.jpg"></div>
                    <div class="item"><img alt="Owl Image" src="essec/e3.jpg"></div>
                    <div class="item"><img alt="Owl Image" src="essec/e4.jpg"></div>
                    <div class="item"><img alt="Owl Image" src="images/essec3.jpg"></div>
                    
 
                </div>
            </div>
        </section>
    </div>
    <div class="page-section  short" style="padding: 17px 0;" id="s6">
        <section class="contact-us yellow contact-us2">
            <div class="container">
                <h1 class="short">NOUS CONTACTER</h1>
                <div class="row">
                    <div class="two-columns">
                        <div class="col-sm-4 col-xs-12 padding">
                            <div class="field-column yellow border contactb">
                               <strong class="real-estate">ADRESSE</strong>
                                <p>Adresse : Campus ESSEC Afrique-Atlantique - Plage des Nations – Golf City
                                    Route de Kénitra - Sidi Bouknadel (Rabat-Salé) – MAROC</p>
                            </div>
                        </div>
                        <div class="col-sm-8 col-xs-12 padding">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4670.084408109419!2d-6.7333613992458625!3d34.139987728837475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda742b6718ea347%3A0xec472cb243d18f15!2sESSEC+Business+School!5e0!3m2!1sen!2s!4v1504087528755" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="page-section white short2">
        <section class="contact-us yellow">
            <div class="container">
                <div class="row">
                    <div class="two-columns">

                        <h1 class="short">Vous avez besoin de plus d'informations ?</h1>
                        <div class="col-sm-6 col-xs-12 padding ginfo">
                            <span class="titleinfo"></span> <span class="tel cinfo"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:+212537824001">+212 (0)5 37 82 40 01</a></span> <span class="mail cinfo"> </span> <span class="website cinfo"><i class="fa fa-globe" aria-hidden="true"></i> <a href="http://www.essec.edu/fr/" target="_blank">www.essec.edu/fr</a></span>
                            <ul id="social-media-etab">
                                <li><a href="https://www.facebook.com/essec" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                <li><a href="https://www.instagram.com/essec_bschool/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                <li><a href="https://www.youtube.com/user/essec" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                <li><a href="https://www.linkedin.com/edu/school?id=12436" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-xs-12 padding">
                            <form action="mail.php" class="contact-form" method="POST">
                                <input placeholder="Nom & Prénom*" type="text" name="full_name"> <input placeholder="Email*" type="email" name="email"> <input placeholder="Sujet" type="text" name="sujet">
                                <textarea placeholder="Message" name="message"></textarea>
                                <div class="holder yellow">
                                    <button type="submit">Envoyer</button> <span><em>*</em>Champs obligatoires</span>
                                </div>
                                <input type="hidden" name="to" value="etore@essec.edu" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php include 'includes/footer.php'?>
<script src="js/jquery.min.js">
</script> <!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="js/owl.carousel.js">
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#owl-demo").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:false,

                }
            }




        });

    });

</script>
<script src="js/bootstrap.min.js">
</script>
<script src="js/switcher.js">
</script>
<script src="js/custom.js">
</script>
</body>
</html>
