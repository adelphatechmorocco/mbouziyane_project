<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>Ecole Centrale Casablanca</title><!-- Bootstrap style sheet -->
	<link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/custom.css" id="style" rel="stylesheet">
	<link href="css/color-blue.css" id="colors" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/switcher.css" rel="stylesheet">
	<link href="css/layout.css" rel="stylesheet">
	<link rel="stylesheet" href="css/responsive.css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="central-casa">
	<!-- main wrapper of the page -->
	<div id="wrapper">
		<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
			</div>
		</div><?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img alt="image" height="157" class="insaban" src="images/ce.png" width="1920">
			<div class="banner-text">
				<h1>Ecole Centrale Casablanca</h1>
				<h3>Inventons le monde de demain</h3>
			</div>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>Ecole Centrale Casablanca</li>
			</ul>
		</div>
		<section class="page-section white short" style="padding: 13px 0;">
			<div class="container">
				<div class="row tool-tip yellow">
					<div class="col-sm-4"><img src="images/cc.png"></div>
					<div class="col-sm-8">
						<p class="text-justify">L’École Centrale Casablanca est la première école d’ingénieur généraliste du Maroc. Une École intégrée au réseau international des Écoles Centrale (France, Chine, Inde et Maroc) dont la pédagogie innovante vise à former des ingénieurs de haut niveau scientifique, dotés d’une culture scientifique pluridisciplinaire de haut niveau.</p>
						<p>La formation centralienne intègre une indispensable expérience internationale. Elle se structure en outre sur sa proximité avec les grandes entreprises et multinationales. Enfin, elle valorise le développement personnel de l’élève-ingénieur, dans une logique de renforcement de ses compétences relationnelles et de son savoir-être. L’ingénieur centralien se distingue ainsi par sa capacité à appréhender la complexité, l’innovation, la responsabilité sociétale, et la diversité culturelle.</p>
					</div>
				</div>
			</div>
		</section>
		<div class="page-section short">
			<div class="container">
				<div class="row">
					<section class="services-tab short col-md-12">
						<div>
							<div class="tab_2 second yellow">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active" role="presentation">
										<a aria-controls="home3" data-toggle="tab" href="#home3" role="tab"><img alt="" class="center-block" src="images/business-people-meeting.png" style="display: block">Formation Proposée</a>
									</li>
									<li role="presentation">
										<a aria-controls="home" data-toggle="tab" href="#home9" role="tab"><img alt="" class="center-block" src="images/diploma.png" style="display: block">Reconnaissance du diplôme</a>
									</li>
									<li role="presentation">
										<a aria-controls="profile" data-toggle="tab" href="#profile" role="tab"><img alt="" class="center-block" src="images/icon-suit.png" style="display: block"> Débouchés professionnels</a>
									</li>
									<li role="presentation">
										<a aria-controls="frais" data-toggle="tab" href="#frais" role="tab"><img class="center-block" src="images/graduate-showing-his-diploma.png" style="display: block">Intégrer notre établissement</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="#" id="linkmsg"><img alt="" class="center-block" src="images/email.png" style="display: block"> Plus d'informations</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="http://www.centrale-casablanca.ma/" target="_blank" id="linkmsg"><img alt="" class="center-block" src="images/www.png" style="display: block"> Site de l’établissement </a>
									</li>
								</ul><!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane fade in active" id="home3" role="tabpanel">
										<p>Cycle Ingénieur Bac +5</p>
										<p>Plus d’infos sur <a href="http://www.centrale-casablanca.ma/" target="_blank">www.centrale-casablanca.ma</a></p>
									</div>
									<div class="tab-pane fade" id="home9" role="tabpanel">
										<p>La Commission Nationale de Coordination de l’Enseignement Supérieur (Cnaces) a statué favorablement pour la reconnaissance de l’Etat du diplôme de l’Ecole Centrale Casablanca.</p>
										<p>Au terme de leur cursus, l’ensemble des lauréats ingénieurs auront la possibilité d’orienter leur carrière tout aussi bien vers le secteur privé que la fonction publique.</p>
									</div>
									<div class="tab-pane fade" id="profile" role="tabpanel">
										<p>Les opportunités professionnelles des Centraliens sont très diversifiées, car la formation Centralienne généraliste autorise des évolutions souples de carrière.</p>
										<p>La majorité des Centraliens évolue à un moment donné vers des fonctions de management ou de direction</p>
									</div>
									<div class="tab-pane fade" id="frais" role="tabpanel">
										<div class="col-xs-12">
											<strong>Procédure de candidature</strong>
											<p>L’accès en 1ère année de l’École Centrale Casablanca s’effectue dans le cadre de deux concours, soit celui réservé aux étudiants des classes préparatoires aux grandes Écoles (CPGE), soit celui ouvert aux étudiants en licence ou maîtrise de mathématiques.</p>
											<strong>Frais scolaire</strong>
											<p>Frais de scolarité : 50000 DRH/an<br>
											Les frais de scolarité sur les 3 premiers semestres peuvent être gratuits ou se situer entre 5 000 et 50 000 Dirhams par an, en fonction du revenu de la famille</p><strong>Aide financière</strong>
											<p>Un étudiant modeste et brillant doit pouvoir effectuer sa scolarité à l’Ecole Centrale Casablanca sans obstacle de nature financière, s’il en a les capacités humaines et intellectuelles. Depuis son lancement, l’École Centrale Casablanca a établi un partenariat avec la Fondation Moulay Youssef reconnue d’utilité publique pour ses actions visant à accompagner les étudiants d’origine modeste, en leur apportant toute l’aide nécessaire à la réussite de leurs études. Pour pouvoir éventuellement bénéficier d’une exonération sur les frais de scolarité et de logement, les élèves doivent effectuer une demande auprès de l’établissement.</p><strong>Logement</strong>
											<p>Les élèves de Centrale Casablanca sont logés en résidence étudiante, située directement sur le campus. Les frais de logement se situent entre la gratuité complète et 10 000 Dirhams par an.</p>
										</div>
										<div class="row tool-tip yellow">
											<div class="col-xs-12">
												<div class="row">
													<div class="col-sm-12 no-padding-left"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="page-section white" style="padding-top: 50px; padding-bottom: 50px">
			<section class="contact-us yellow">
				<div class="container">
					<div class="owl-theme cetraleslide" id="owl-demo">
						<div class="item"><img alt="Owl Image" src="images/CampusCentrale1.JPG"></div>
						<div class="item"><img alt="Owl Image" src="images/CampusCentrale2.JPG"></div>
						<div class="item"><img alt="Owl Image" src="images/CampusCentrale3.JPG"></div>

					</div>
				</div>
			</section>
		</div>
		<div class="page-section short" id="s6" style="padding: 12px 0;">
			<section class="contact-us yellow contact-us2">
				<div class="container">
					<h1 class="short">NOUS CONTACTER</h1>
					<div class="row">
						<div class="two-columns">
							<div class="col-sm-4 col-xs-12 padding">
								<div class="field-column yellow border contactb">
									<strong class="real-estate">ADRESSE</strong>
									<p>Adresse : Bouskoura Ville Verte Bouskoura 27182 – MAROC</p>
								</div>
							</div>
							<div class="col-sm-8 col-xs-12 padding">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13311.747591405323!2d-7.607588621582031!3d33.47699362090381!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6c5587c807a6f58e!2sEcole+Centrale+Casablanca!5e0!3m2!1sen!2s!4v1502096742295"></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="page-section short2">
			<section class="contact-us yellow">
				<div class="container">
					<div class="row">
						<div class="two-columns">
							<h1 class="short">Vous avez besoin de plus d'informations ?</h1>
							<div class="col-sm-6 col-xs-12 padding ginfo">
								<span class="titleinfo"></span> <span class="tel cinfo"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:+212522493500">+212 (0) 5 22 49 35 00</a></span> <span class="mail cinfo"></span> <span class="website cinfo"><i aria-hidden="true" class="fa fa-globe"></i> <a href="http://www.centrale-casablanca.ma/" target="_blank">www.centrale-casablanca.ma</a></span>
								<ul id="social-media-etab">
									<li>
										<a href="https://www.facebook.com/centralecasablanca" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a href="https://twitter.com/centralecasa" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a>
									</li>
									<li>
										<a href="https://www.linkedin.com/edu/school?id=372026" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a>
									</li>
									<li>
										<a href="https://www.youtube.com/user/centralecasablanca" target="_blank"><i aria-hidden="true" class="fa fa-youtube"></i></a>
									</li>
								</ul>
							</div>
							<div class="col-sm-6 col-xs-12 padding">
                            <form action="mail.php" class="contact-form" method="POST">
                                <input placeholder="Nom & Prénom*" type="text" name="full_name"> <input placeholder="Email*" type="email" name="email"> <input placeholder="Sujet" type="text" name="sujet">
                                <textarea placeholder="Message" name="message"></textarea>
                                <div class="holder yellow">
                                    <button type="submit">Envoyer</button> <span><em>*</em>Champs obligatoires</span>
                                </div>
                                <input type="hidden" name="to" value="contacts@centrale-casablanca.ma" />
                            </form>
                        </div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div><?php include 'includes/footer.php'?>
	<script src="js/jquery.min.js">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->

	<script src="js/owl.carousel.js">
	</script>
	<script type="text/javascript">
	              $(document).ready(function() {

	                  $("#owl-demo").owlCarousel({
	                autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

	                      responsive: {
	                          0: {
	                              items: 1,
	                              nav: false
	                          },
	                          600: {
	                              items: 2,
	                              nav: false
	                          },
	                          1000: {
	                              items: 3,
	                              nav: false,

	                          }
	                      }

	                  });

	              });
	</script>
	<script src="js/bootstrap.min.js">
	</script>
	<script src="js/switcher.js">
	</script>
	<script src="js/custom.js">
	</script>
</body>
</html>
