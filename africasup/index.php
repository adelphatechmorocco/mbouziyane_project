﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- title of the page -->
    <title>Africa Sup, Grandes Ecoles France -Maroc </title>

    <!-- Bootstrap style sheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- css style sheet -->
    <link id="style" rel="stylesheet" href="css/style.css">
    <link id="style" rel="stylesheet" href="css/custom.css">



    <link id="colors" rel="stylesheet" href="css/color-blue.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/switcher.css">

    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link
        href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700"
        rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>


<!-- main wrapper of the page -->
<div id="wrapper">
    <div class="top-bar4">
        <div class="container">
            <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
        </div>
    </div>

    <?php include 'includes/header.php' ?>

    <div class="justics-holder">
        <div id="carousel-example-generic102" class="carousel slide" >

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">


                        <div class="item">
                    <div align="center">
			               <video id="player" class="myvideo" controls autoplay>
			                 <source src="http://africasup.org/videos/Africa Sup (V4) FHD.m4v" width="1024" height="552" type="video/mp4">
			                 Your browser does not support the video tag.
			              </video>
                    </div>
                </div>

                       <div class="item">
                        <div class="justices-img">
                        <img src="images/s1.png" width="1920" height="503" alt="image" class="img imgsld" style="height: 530px !important">
                        <div class="justics-text">
                   
                        </div>
                    </div>
                    </div>


        <!--    <div class="item">
                    <div class="justices-img">
                        <img src="sup.jpg" style="height: 530px;" width="1920" height="1060" alt="image" class="img imgsld" id="sup">
                        <img src="min.jpg"  width="1920" style="height: 530px;" alt="image" class="img imgsld" id="min">
                         <img src="phone.jpg"  alt="image" class="img imgsld" id="phone">
                    </div>
                </div>-->
                
                <div class="item">
                    <div class="justices-img">
                        <img src="images/africa/EMLYON_S.jpg" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>EmLyon Business School Campus de Casablanca </h1>
                                <a class="more courses" href="http://africasup.org/emlyon.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/ESSEC-slider.png" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>ESSEC Afrique-Atlantique</h1>
                                <a class="more courses" href="http://africasup.org/ESSEC.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/africa/EMINES_S.jpg" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>EMINES - School of Industrial Management</h1>
                                <a class="more courses" href="http://africasup.org/EMINES.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/CampusCentrale1.JPG" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>Ecole Centrale Casablanca </h1>
                                <a class="more courses" href="http://africasup.org/centrale-casa.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/africa/INSA_S.jpg" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>INSA Euro-Méditerranée</h1>
                                <a class="more courses" href="http://africasup.org/INSA.php">Voir
                                    Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control prev-2" href="#carousel-example-generic102" role="button" data-slide="prev">
                <span><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control next-2" href="#carousel-example-generic102" role="button"
               data-slide="next">
                <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>


    <div class="page-section block yellow white" id="s1">

        <div class="container">

            <section class="welcome yellow">
                <div class="col-xs-12 heading-holder">


                    <h1><img src="images/house-outline.png"> Qui sommes-nous ?</h1>

                </div>

                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1.5s">
                    <img class="blockimg" src="images/block3.png">
                    <p> Des formations d’excellence au Maroc
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1s">
                    <img class="blockimg" src="images/block4.png">
                    <p>Une reconnaissance en France, au Maroc et à l’International </p>
                </div>
                <!-- plante-->
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="0.5s">
                    <img class="blockimg" src="images/africa.png">
                    <p>Des écoles tournées vers l’Afrique et les enjeux du continent</p>
                </div>

                <br/><br/>

                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1.5s">
                    <img class="blockimg" src="images/block2.png">
                    <p>Le multiculturalisme
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1s">
                    <img class="blockimg" src="images/block1.png">
                    <p>L’esprit d’entreprendre
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="0.5s">
                    <img class="blockimg" src="images/idea.png">
                    <p>L’innovation et la créativité

                    </p>
                </div>
            </section>
        </div>
    </div>


    <div class="page-section  yellow">
        <div class="container">
            <section class="welcome yellow">
                <div class="row">
                    <div class="welcome-d col-sm-8 col-xs-12">






                        <p class="text-justify"><strong> Bienvenue sur Africa-Sup,</strong> le premier regroupement d’établissements d’excellence lancé dans le cadre d’un partenariat entre la France et le Maroc !</p>
                        <p class="text-justify spacep">Africa Sup est composée de <strong>5 écoles d’ingénieurs et de commerce installées au Maroc, qui partagent l’ambition et la volonté de s’ouvrir au continent africain :</strong> Ecole Centrale Casablanca, EMINES - School of Industrial Management au sein de l’Université Mohammed VI Polytechnique, emlyon business school campus Casablanca, ESSEC campus Afrique - Atlantique et INSA Euro-Méditerranée au sein de l’Université Euro-Méditerranéenne de Fès.
                        </p>
                        <p class="text-justify spacep">Tous nos membres sont soutenus, à des niveaux divers, par les <strong>gouvernements marocains et français.</strong> Ils disposent tous de forts liens avec des établissements d’enseignement supérieur français, certains s’inscrivant dans le développement international de leur groupe en France, d’autres disposant de doubles diplômes avec des partenaires français.
                        </p>
                        <p class="text-justify spacep">Les établissements d’Africa Sup partagent la même vision en termes de <strong>qualité de leur enseignement, de valeurs communes et de formations inclusives.</strong> Ils souhaitent travailler ensemble dans une approche Sud-Sud soucieuse de diffuser leur formation dans un contexte multiculturel et de participer au développement du continent africain.</p>
                        <p class="text-justify spacep">
L’Ambassade de France au Maroc a souhaité soutenir et promouvoir la plateforme Africa Sup qui regroupe des formations d’excellence au Maroc.</p>
                    </div>
                    <div class="col-sm-4 col-xs-12 welcome-img">

                        <img src="images/Africa sup -web (10).jpg" width="448" height="428" alt="image">
                    </div>
                </div>
            </section>
        </div>
    </div>


    <div class="page-section white" id="s2">
        <div class="container">
            <div class="row">
                <section class="p_courses">
                    <div class="col-xs-12 heading-holder">
                        <h1><img class="princip" src="images/princip.png"> Notre vision</h1>

                    </div>
                    <div class="c_column">
                         <!-- <p>Le projet Africa Sup est résolument ambitieux. Il vise :</p> -->
                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i> <b>Des formations de très haut niveau :</b>  Les établissements d’Africa Sup ont pour mission de former les leaders de demain en Afrique. L’ensemble de nos écoles souhaitent se développer en lien et avec le continent africain pour répondre aux enjeux de ce continent en pleine evolution.
                        </p>
                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i> <b>Une vision internationale partagée :</b> Les Écoles membres d’Africa Sup figurent parmi les établissements d’excellence au Maroc et au niveau international. Elles partagent l’ambition de s’ouvrir à l’international en développant de nombreux partenariats à l’étranger ou encore en installant des campus dans les pays et continents émergents qui compteront demain.
 
                        </p>
                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i><b> La diversité et l’ouverture :</b> La diversité est un enjeu pour progresser collectivement. Les écoles d’Africa Sup se distinguent par leur capacité à appréhender la diversité culturelle et la responsabilité sociétale.
                        </p>

                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i><b> Esprit d’entreprendre, innovation et créativité : </b> Les écoles d’Africa Sup allient compétences scientifiques et rigueur académique avec créativité et esprit d’entreprendre, incitant leurs étudiants à relever les défis économiques, managériaux, sociaux, environnementaux et éthiques de demain.  Cette approche permet à leurs étudiants d’appréhender rapidement le monde du travail.
                        </p>
                    </div>

                </section>
            </div>
        </div>
    </div>


    <div class="page-section" id="s3">
        <div class="container">
            <div class="row">
                <section class="special mot-africasup" id="team">
                    <div class="heading-area">
                        <h1 class="text-center">Le mot de l’Ambassade de France au Maroc</h1>
                    </div>
                    <div id="carousel-example-generics" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                        <img src="images/logoaf.jpg" style="height: 276px; border: 1px solid; margin-bottom: 10px;" width="310" height="400" alt="image" class="allign-left modeimg">
                            <div class="item active">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 s-block s-1">
                                        <div class="s-holder">
                                            <p class="text-justify">Le Maroc « Hub Universitaire pour l’Afrique » : voilà une formule qui aujourd’hui s’incarne dans la coopération avec la France grâce à Africa Sup !</p><br>
 <p class="text-justify">
L’Ambassade de France soutient, encourage et promeut cette initiative qui vise à faire mieux connaître en Afrique ces formations d’excellence, ces Grandes Ecoles qui ont choisi de se développer au Maroc en relation avec la France.</p><br>
 <p class="text-justify">
Les étudiants d’Afrique subsaharienne sont de plus en plus mobiles, on en comptait plus de 330 000 en mobilité internationale en 2015. Et près de 70 000 d’entre eux choisissent de rester sur le continent africain. En tant que troisième pays d’accueil de ces étudiants, le Maroc est devenu pour eux une destination de choix grâce à la qualité de ses formations universitaires publiques et privées.</p><br>
 <p class="text-justify">
Africa Sup est conçu pour ces étudiants, pour leur permettre de trouver au Maroc l’excellence française des Grandes Ecoles d’ingénieurs ou de commerce ! 
</p>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="page-section  short" style="padding-top: 50px; padding-bottom: 50px">
        <section class="contact-us yellow">

            <div class="container">

                <h1 class="short"><img src="images/inst.png"> Nos Etablissements</h1>
                <div id="owl-demo" class="owl-theme">

                    <div class="item" style="border:solid 1px #ccc"><a href="http://www.essec.edu/en/"><img src="images/ESSEC1.png"
                                                                              alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.emines-ingenieur.org"><img src="images/EMINES.JPG"
                                                                              alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.centrale-casablanca.ma/"><img src="images/cc1.jpg"
                                                                                        alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.insa-euromediterranee.org/fr/index.html"><img
                                src="images/INSA.jpg" alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.em-lyon.com/en"><img
                                src="images/logo-emlyon.png" alt="Owl Image"></a></div>


                </div>


            </div>


        </section>
    </div>

        <div class="page-section  short" style="padding-top: 50px; padding-bottom: 50px">
        <section class="contact-us yellow">

            <div class="container">

                <h1 class="short"><img src="images/icon-galerie.png"> Galerie</h1>
                <div id="owl-galerie" class="owl-theme">

                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g10.png"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g11.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g12.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g13.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g14.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g15.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g16.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g17.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g18.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g19.jpg"  alt="Owl Image"></div>

                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g20.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g21.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g22.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g23.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g24.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g25.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g26.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g27.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="galerie/g28.jpg"  alt="Owl Image"></div>
           

                </div>

</div>
            


        </section>
    </div>
    <div class="page-section">
        <section class="map" style="pointer-events:none; border:0;" >
            <iframe src="https://www.google.com/maps/d/embed?mid=1_1pUF3KXksR-os9sdB12qW91Bso" width="640" height="480"
                    frameborder="0" scrolling="no"></iframe>
        </section>
    </div>

    <?php include 'includes/footer.php' ?>
</div>


<div id="scroll-top">
    <i class="fa fa-chevron-up fa-3x"></i>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/owl.carousel.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 4,


                }
            }


        });


                $("#owl-galerie").owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 4,


                }
            }


        });

    });

    var randomSlide = Math.floor(Math.random() * $('#carousel-example-generic102 .item').size());

    jQuery(document).ready(function($) {
        $('#carousel-example-generic102').carousel(randomSlide);
        $('#carousel-example-generic102').carousel('next');
    });


</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/switcher.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/custom.js"></script>
<script>
    new WOW().init();
</script>

</body>
</html>
