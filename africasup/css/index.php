﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- title of the page -->
    <title>Africa Sup, Grandes Ecoles France -Maroc </title>

    <!-- Bootstrap style sheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- css style sheet -->
    <link id="style" rel="stylesheet" href="css/style.css">
    <link id="style" rel="stylesheet" href="css/custom.css">



    <link id="colors" rel="stylesheet" href="css/color-blue.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/switcher.css">

    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link
        href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700"
        rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<!-- main wrapper of the page -->
<div id="wrapper">
    <div class="top-bar4">
        <div class="container">
            <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
        </div>
    </div>

    <?php include 'includes/header.php' ?>

    <div class="justics-holder">
        <div id="carousel-example-generic102" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item">
                    <div class="justices-img">
                        <img src="images/sup.png" width="1920" height="1060" alt="image" class="img imgsld">

                    </div>
                </div>

                <div class="item">
                    <div class="justices-img">
                        <img src="images/africa/EMLYON_S.jpg" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>EmLyon Business School Campus de Casablanca </h1>
                                <a class="more courses" href="http://africasup.org/emlyon.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="essec/ESSEC photo.JPG" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>ESSEC Afrique-Atlantique</h1>
                                <a class="more courses" href="http://africasup.org/ESSEC.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/africa/EMINES_S.jpg" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>EMINES - School of Industrial Management</h1>
                                <a class="more courses" href="http://africasup.org/EMINES.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/CampusCentrale1.JPG" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>Ecole Centrale Casablanca </h1>
                                <a class="more courses" href="http://africasup.org/centrale-casa.php">Voir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="justices-img">
                        <img src="images/africa/INSA_S.jpg" width="1920" height="1060" alt="image" class="img imgsld">
                        <div class="justics-text">
                            <div class="txt">
                                <h1>INSA Euro-Méditerranée</h1>
                                <a class="more courses" href="http://africasup.org/INSA.php">Voir
                                    Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control prev-2" href="#carousel-example-generic102" role="button" data-slide="prev">
                <span><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control next-2" href="#carousel-example-generic102" role="button"
               data-slide="next">
                <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>


    <div class="page-section block yellow white" id="s1">

        <div class="container">

            <section class="welcome yellow">
                <div class="col-xs-12 heading-holder">


                    <h1><img src="images/house-outline.png"> Qui sommes-nous ?</h1>

                </div>

                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1.5s">
                    <img class="blockimg" src="images/block3.png">
                    <p> Des formations d’excellence au Maroc
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1s">
                    <img class="blockimg" src="images/block4.png">
                    <p>Une reconnaissance en France, au Maroc et à l’International </p>
                </div>
                <!-- plante-->
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="0.5s">
                    <img class="blockimg" src="images/africa.png">
                    <p>Des écoles tournées vers l’Afrique et les enjeux du continent</p>
                </div>

                <br/><br/>

                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1.5s">
                    <img class="blockimg" src="images/block2.png">
                    <p>Le multiculturalisme
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="1s">
                    <img class="blockimg" src="images/block1.png">
                    <p>L’esprit d’entreprendre
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInLeft AfriqueSup-item" data-wow-duration="0.5s">
                    <img class="blockimg" src="images/idea.png">
                    <p>L’innovation et la créativité

                    </p>
                </div>
            </section>
        </div>
    </div>


    <div class="page-section  yellow">
        <div class="container">
            <section class="welcome yellow">
                <div class="row">
                    <div class="welcome-d col-sm-8 col-xs-12">






                        <p class="text-justify"><strong>Bienvenue sur la plateforme Africa-Sup,</strong> le premier regroupement d’établissements d’excellence lancé dans le cadre d’un partenariat entre la France et le Maroc !</p>
                        <p class="text-justify spacep">Africa Sup est composée de <strong>5 écoles d’ingénieurs et de commerce installées au Maroc, qui partagent l’ambition et la volonté de s’ouvrir au continent africain :</strong> Ecole Centrale Casablanca, EMINES - School of Industrial Management au sein de l’Université Mohammed VI Polytechnique, emlyon business school campus Casablanca, ESSEC campus Afrique - Atlantique et INSA Euro-Méditerranée au sein de l’Université Euro-Méditerranéenne de Fès.
                        </p>
                        <p class="text-justify spacep">Tous nos membres sont soutenus, à des niveaux divers, par les <strong>gouvernements marocains et français.</strong> Ils disposent tous de forts liens avec des établissements d’enseignements supérieurs français, certains s’inscrivant dans le développement international de leur groupe en France, d’autres disposant de doubles diplômes avec des partenaires français.
                        </p>
                        <p class="text-justify spacep">Les établissements d’Africa Sup partagent la même vision en termes de <strong>qualité de leur enseignement, de valeurs communes et de formations inclusives.</strong> Ils souhaitent travailler ensemble dans une approche Sud-Sud soucieuse de diffuser leur formation dans un contexte multiculturel et de participer au développement du continent africain.</p>
                        <p class="text-justify spacep">
L’Ambassade de France au Maroc a souhaité soutenir et promouvoir la plateforme Africa Sup qui regroupe des formations d’excellence au Maroc.</p>
                    </div>
                    <div class="col-sm-4 col-xs-12 welcome-img">

                        <img src="images/people1.jpg" width="448" height="428" alt="image">
                    </div>
                </div>
            </section>
        </div>
    </div>


    <div class="page-section white" id="s2">
        <div class="container">
            <div class="row">
                <section class="p_courses">
                    <div class="col-xs-12 heading-holder">
                        <h1><img class="princip" src="images/princip.png"> Nos valeurs</h1>

                    </div>
                    <div class="c_column">
                         <p>Le projet Africa Sup est résolument ambitieux. Il vise :</p>
                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i> Un positionnement de ses
                            membres comme <b>leaders sur le continent africain</b>
                        </p>
                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i> Des <b>formations de très
                                haut niveau pour former les leaders de demain pour
                                l’Afrique </b>: ce regroupement d’établissements, soutenu par l’Ambassade de France, est
                            garant de la qualité des formations
                        </p>
                        <p class="text-justify"><i class="fa fa-check" aria-hidden="true"></i> <b>Un objectif de
                                rayonnement sur le territoire national et sur le continent :</b> Les
                            membres d’Africa Sup bénéficieront d’un large ensemble universitaire, qui leur donnera
                            une meilleure visibilité à leurs diplômes au Maroc et en Afrique (présence sur des
                            salons étudiants et autres rencontres universitaires en Afrique, nouer des partenariats
                            avec des établissements d’enseignement supérieur en Afrique, concours communs…)
                        </p>
                    </div>

                </section>
            </div>
        </div>
    </div>


    <div class="page-section" id="s3">
        <div class="container">
            <div class="row">
                <section class="special mot-africasup" id="team">
                    <div class="heading-area">
                        <h1 class="text-center">mot de ...</h1>
                    </div>
                    <div id="carousel-example-generics" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 s-block s-1">
                                        <img src="images/motP.jpg" width="310" height="400" alt="image"
                                             class="allign-left">
                                        <div class="s-holder">
                                            <span>DR. JULIE THOMAS</span>
                                            <em>Dental Surgerien</em>
                                            <p class="text-justify">Praesent sapien massa, convallis a pellentesque nec,
                                                egestas non nisi.

                                                Nulla porttitor accumsan tincidunt.

                                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                                                cubilia Curae; Donec velit neque, auctor sit amet aliquam vel,
                                                ullamcorper sit amet ligula.

                                                Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="page-section  short" style="padding-top: 50px; padding-bottom: 50px">
        <section class="contact-us yellow">

            <div class="container">

                <h1 class="short"><img src="images/inst.png"> Nos Etablissements</h1>
                <div id="owl-demo" class="owl-theme">

                    <div class="item" style="border:solid 1px #ccc"><a href="http://www.essec.edu/en/"><img src="images/ESSEC1.png"
                                                                              alt="Owl Image"></a></div>
                    <div class="item"><a href="www.emines-ingenieur.org"><img src="images/EMINES.JPG"
                                                                              alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.centrale-casablanca.ma/"><img src="images/cc1.jpg"
                                                                                        alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.insa-euromediterranee.org/fr/index.php"><img
                                src="images/INSA.jpg" alt="Owl Image"></a></div>
                    <div class="item"><a href="http://www.em-lyon.com/en"><img
                                src="images/logo-emlyon.png" alt="Owl Image"></a></div>


                </div>


            </div>


        </section>
    </div>
    <div class="page-section">
        <section class="map" style="pointer-events:none; border:0;" >
            <iframe src="https://www.google.com/maps/d/embed?mid=1_1pUF3KXksR-os9sdB12qW91Bso" width="640" height="480"
                    frameborder="0" scrolling="no"></iframe>
        </section>
    </div>

    <?php include 'includes/footer.php' ?>
</div>


<div id="scroll-top">
    <i class="fa fa-chevron-up fa-3x"></i>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/owl.carousel.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 4,


                }
            }


        });

    });

    var randomSlide = Math.floor(Math.random() * $('#carousel-example-generic102 .item').size());

    jQuery(document).ready(function($) {
        $('#carousel-example-generic102').carousel(randomSlide);
        $('#carousel-example-generic102').carousel('next');
    });


</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/switcher.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/custom.js"></script>
<script>
    new WOW().init();
</script>

</body>
</html>
