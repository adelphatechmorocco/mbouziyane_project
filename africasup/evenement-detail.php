<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- title of the page -->
<title>Événement Detail</title>

<!-- Bootstrap style sheet -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- css style sheet -->
<link rel="stylesheet" href="css/style.css">
<link id="style" rel="stylesheet" href="css/custom.css">

<link id="colors" rel="stylesheet" href="css/color-blue.css">
<link rel="stylesheet" href="css/switcher.css">
        
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"> 

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

 
<!-- main wrapper of the page -->
<div id="wrapper"> 
	<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png" 
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png" 
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>

			</div>
		</div>

	<?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img src="images/ce.png" width="1920" height="157" alt="image">
			<div class="banner-text">
				<h2>ADONEC RUTRUM CONGUE LEO EGET MALESUADA</h2>
				<ul class="breadcrumb yellow">
					<li><a href="http://africasup.org/">home</a></li>
					<li>Événement Detail</li>
				</ul>
			</div>
		</div>
	
	<div id="main">
		<div class="container">
			<div class="col-xs-12">
				<div class="row">
					<section class="latest-news inner yellow">
						<div class="row">
							<div class="col-xs-12">
								<div class="img-holder">
									<img src="images/slide1.jpg" width="370" height="300" alt="image">
									<span class="date">
										16
										<em>Août</em>
									</span>
								</div>
								<div class="news-text">
									<span class="title">Sed porttitor lectus nibh.</span>
									<p>Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.</p>
									<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla porttitor accumsan tincidunt. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt. Donec rutrum congue leo eget malesuada.</p>
									
									<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.</p>


									<div class="tags yellow">
										<span>Mots clés :</span>
										<a href="#">Vestibulum,</a>
										<a href="#">Pellentesque,</a>
										<a href="#">cubilia,</a>
										<a href="#">elementum</a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>

	<?php include 'includes/footer.php'?>
			
		</div>

 

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/switcher.js"></script>
	<script src="js/custom.js"></script>		

	
	</body>
</html>
