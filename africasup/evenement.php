<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport"><!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<!-- title of the page -->
	<title>Événements & Actualités</title><!-- Bootstrap style sheet -->
	<link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/custom.css" id="style" rel="stylesheet">
	<link href="css/color-blue.css" id="colors" rel="stylesheet">
	<link href="css/switcher.css" rel="stylesheet">
	<link href="css/font-awesome.css" rel="stylesheet">
	   <link rel="stylesheet" href="css/owl.carousel.css">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- main wrapper of the page -->
	<div id="wrapper">
		<div class="top-bar4">
			<div class="container">
				<div class="logo">
					<a href="http://africasup.org/"><img alt="descipline" class="img-responsive" src="images/logo45.png"></a>
				</div>
				<div class="logomobile">
					<a href="http://africasup.org/"><img alt="descipline" class="img-responsive" src="images/logo44.png"></a>
				</div>
				<div class="logotitle">
					Le premier regroupement de Grandes Écoles soutenues par la France au Maroc
				</div>
			</div>
		</div><?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img alt="image" height="157" src="images/ce.png" width="1920">
			<div class="banner-text">
				<h1>Événements & Actualités</h1>
			</div>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>Événements & Actualités</li>
			</ul>
		</div>
		<div class="page-section white">

			<div class="container">
				<div class="row">
					<section class="p_courses yellow">
						<div class="update-holder">
							<div class="update-box">
                                <div class="row">
									<div class="col-md-5 col-xs-12 img-block"> 
										<a href="evenement-detail4.php"><img alt="image" class="img-event" height="260" src="images/Logo-1.png" width="570"></a>
									</div>
									<div class="col-md-7 col-xs-12">
										<div class="text-block">
											<div class="holder_1">
												<span class="date">20 au 21 octobre 2017, Abidjan - Côte d’Ivoire</span>
											</div><span class="title">Tournée de l'Ouest Africain :  Conférences et rencontres avec les étudiants ivoiriens    </span>
											<p>Africa Sup continu sa tournée à Abidjan. Une équipe de l’emlyon Casablanca sera présente aux « Conférences et rencontres avec les étudiants ivoiriens », le 20 et 21 octobre à Abidjan.
											Cette tournée présentera à la fois un volet étudiant et un volet institutionnel qui sera l'occasion pour nos établissements d'enseignement supérieur de nouer de futurs partenariats.</p>
											<a href="">http://www.campusfrance.org/fr/evenement/tourn%C3%A9e-de-louest-africain-s%C3%A9n%C3%A9gal-et-c%C3%B4te-divoire-du-17-au-21-octobre-2017</a>
											<div class="register-holder">
												<!--<a href="evenement-detail4.php">LIRE LA SUITE</a>-->
											</div>
										</div>
									</div>
								</div>
                                <br/>
								<br/>
								<div class="row">
									<div class="col-md-5 col-xs-12 img-block">
										<a href="evenement-detail3.php"><img alt="image" class="img-event" height="260" src="images/https _cdn.evbuc.com_images_36045690_226408793643_1_original.jpg" width="570"></a>
									</div>
									<div class="col-md-7 col-xs-12">
										<div class="text-block">
											<div class="holder_1">
												<span class="date">17 au 18 octobre 2017, Dakar - Sénégal</span>
											</div><span class="title">Tournée de l'Ouest Africain :  Salon « formations et 1er emploi » au Dakar.</span>
											<p>L’INSA euro-med et l’emlyon Casablanca seront présents ensembles à Dakar sur le salon « formations et 1er emploi » du 17 au 18 octobre. Ce Salon offre aux jeunes en phase d’insertion professionnelle une opportunité unique de rencontrer des établissements d’enseignement supérieur français et africains, des conseillers d’orientation, des entreprises, des spécialistes de l’emploi et du développement personnel. Cet évènement permettra à nos établissements d'enseignement supérieur de nouer de futurs partenariats.</p>
											<a href="http://www.senegal.campusfrance.org/actualite/salon-formations-et-1er-emploi">http://www.senegal.campusfrance.org/actualite/salon-formations-et-1er-emploi</a>
											<div class="register-holder">
												<!--<a href="evenement-detail3.php">LIRE LA SUITE</a>-->
											</div>
										</div>
									</div>
								</div>
								<br/>
								<br/>

								<div class="row">
									<div class="col-md-5 col-xs-12 img-block">
										<a href="evenement-detail2.php"><img alt="image" class="img-event" height="260" src="images/africa.jpg" width="570"></a>
									</div>
									<div class="col-md-7 col-xs-12">
										<div class="text-block">
											<div class="holder_1">
												<span class="date">12 au 15 octobre 2017, Lagos - Nigeria</span>
											</div><span class="title">Africa Sup au Nigeria</span>
											<p>A l’occasion du Tony Elumelu Foundation Entrepreneurship Forum (rencontre de l’entrepreneuriat en Afrique) qui a lieu le 13 et 14 octobre à Lagos, des membres de l’emlyon Casablanca seront présents au Nigeria pour présenter Africa Sup aux entrepreneurs et futurs étudiants.</p>
											<p>Ce forum rassemble des entrepreneurs en provenance de 54 pays africains ainsi que les leaders du monde des affaires africain.</p>
											<div class="register-holder">
												<!--<a href="evenement-detail2.php">LIRE LA SUITE</a>-->
											</div>
										</div>
									</div>
								</div>
								<br/>
								<br/>

								<div class="row">
									<div class="col-md-5 col-xs-12 img-block">
										<a href="evenement-detail1.php"><img alt="image" class="img-event" height="260" src="images/Africa sup -web (10).jpg" width="570"></a>
									</div>
									<div class="col-md-7 col-xs-12">
										<div class="text-block">
											<div class="holder_1">
												<span class="date">27 septembre 2017, Rabat</span>
											</div>
											<span class="title">Signature et lancement d’Africa Sup</span>
											<p>A l’occasion de la signature de la convention Africa Sup, Monsieur l’Ambassadeur Jean-François Girault reçoit les représentants des cinq écoles membres, leurs étudiants et des Ambassadeurs des pays africains francophones, le 27 septembre 2017 à Rabat. Cette signature marque le lancement du réseau Africa Sup.</p>
											<div class="register-holder">
												<!--<a href="evenement-detail1.php">LIRE LA SUITE</a>-->
											</div>
										</div>
									</div>
								</div>
								
								
								
								
							</div><!--
                                    <div class="col-xs-12">
                            <ul class="pagination inner yellow">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                            </ul>
                        </div>
                        -->
						</div>
					</section>
				</div>
				<br/>
				<br/>

		        <section class="contact-us yellow">

            <div class="container">

                <h1 class="short"><img src="images/icon-galerie.png"> ÉVÉNEMENTS</h1>
                <div id="owl-event" class="owl-theme">

                    
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e1.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e2.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e3.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e4.jpg"  alt="Owl Image"></div>
         
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e6.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e7.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e8.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e9.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e10.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e11.jpg"  alt="Owl Image"></div>
                     <div class="item" style="border:solid 1px #ccc"><img src="events/e12.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e13.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e14.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e15.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e16.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e17.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e18.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e19.jpg"  alt="Owl Image"></div>
                    <div class="item" style="border:solid 1px #ccc"><img src="events/e20.jpg"  alt="Owl Image"></div>

           

                </div>

</div>
            


        </section>
        <br>
        <br>
        <br>
			</div>
		</div><?php include 'includes/footer.php'?>
	</div><!-- Modal -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->
	
	<script src="js/bootstrap.min.js">
	</script> 
	<script src="js/switcher.js">
	</script> 
	<script src="js/cusAum.js"></script>
 <script src="js/owl.carousel.js"></script>
	<script>


$(document).ready(function () {
                $("#owl-event").owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 3,


                }
            }


        });
})
	</script>
</body>
</html>