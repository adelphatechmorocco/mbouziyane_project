<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>INSA Euro-Méditerranée</title><!-- Bootstrap style sheet -->
	<link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/custom.css" id="style" rel="stylesheet">
	<link href="css/color-blue.css" id="colors" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/switcher.css" rel="stylesheet">
	<link href="css/layout.css" rel="stylesheet">
	    <link rel="stylesheet" href="css/responsive.css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="central-casa">
	<!-- main wrapper of the page -->
	<div id="wrapper">
		<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
			</div>
		</div><?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img alt="image" height="157" class="insaban" src="images/ce.png" width="1920">
			<div class="banner-text">
				<h1>INSA Euro-Méditerranée</h1>
				<h3>Devenez ingénieur-e et pas seulement</h3>
			</div>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>INSA Euro-Méditerranée</li>
			</ul>
		</div>
		<section class="page-section white short" style="padding: 13px 0;">
			<div class="container">
				<div class="row tool-tip yellow">
					<div class="col-sm-4"><img src="images/Logo_INSASans-dvpEuromed-RVB.jpg"></div>
					<div class="col-sm-8">
						<p class="text-justify"> L'INSA Euro-Méditerranée est un établissement de l'Université Euromed de Fès placée sous la Haute Présidence d'Honneur de Sa majesté le Roi Mohammed VI.</p>

<p>L’INSA Euro-Méditerranée, 1er institut euro-méditerranéen de formation d'ingénieurs, multi-culturel et multilingue, est membre du Groupe INSA en France (Instituts Nationaux des Sciences Appliquées). C’est le premier INSA à l’international.</p>


<ul>
	<li> <p>Son ambition : former des cadres de haut niveau en capacité d’accompagner de grands projets de développement dans les pays concernés.</p></li>
</ul>



<ul>
	<li> <p>Ses points forts : international, multiculturel, ouvert, innovant en matière de pédagogie et du numérique, partenaire des entreprises et impliqué dans le développement socio-économique euro-méditerranéen.</p> </li>
</ul>


<p class="text-justify">L’Institut se base sur les meilleures pratiques internationales en formation et en recherche. Les enseignements sont assurés par une équipe pédagogique constituée des enseignants des INSA de France, de l’UEMF, des établissements membres du consortium et des professionnels socio-économiques.
</p>

					</div>
				</div>
			</div>
		</section>
		<div class="page-section short">
			<div class="container">
				<div class="row">
					<section class="services-tab short col-md-12">
						<div>
							<div class="tab_2 second yellow">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active" role="presentation">
										<a aria-controls="home3" data-toggle="tab" href="#home3" role="tab"><img alt="" class="center-block" src="images/business-people-meeting.png" style="display: block">Formation Proposée</a>
									</li>
									<li role="presentation">
										<a aria-controls="home" data-toggle="tab" href="#home9" role="tab"><img alt="" class="center-block" src="images/diploma.png" style="display: block">Reconnaissance du diplôme</a>
									</li>
									<li role="presentation">
										<a aria-controls="profile" data-toggle="tab" href="#profile" role="tab"><img alt="" class="center-block" src="images/icon-suit.png" style="display: block"> Débouchés professionnels</a>
									</li>
									<li role="presentation">
										<a aria-controls="frais" data-toggle="tab" href="#frais" role="tab"><img class="center-block" src="images/graduate-showing-his-diploma.png" style="display: block">Intégrer notre établissement</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="#" id="linkmsg"><img alt="" class="center-block" src="images/email.png" style="display: block"> Plus d'informations</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="http://www.insa-euromediterranee.org/" target="_blank" id="linkmsg"><img alt="" class="center-block" src="images/www.png" style="display: block"> Site de l’établissement</a>
									</li>
								</ul><!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane fade in active" id="home3" role="tabpanel">
										<p>L’INSA Euro-Méditerranée dispense une formation d’ingénieur en 5 ans après le Bac. Cet institut a pour ambition d’accueillir des étudiants du monde entier. Les enseignements sont donnés en Français et en anglais.</p>

										<p>Les objectifs généraux de la première année sont la maîtrise des disciplines scientifiques fondamentales. Une part importante est dédiée aux sciences humaines et sociales avec un focus particulier sur la culture euro-méditerranéenne. Le sport est également une activité obligatoire.</p>

										<p>L’entrée en spécialité se fait au début de la deuxième année, avec une part d’enseignements métier qui va croissant au fur et à mesure des années. La troisième année se fait obligatoirement en Europe. De nombreuses période de stages en entreprises ou en laboratoires ponctuent les années d’études. </p>
		<img src="images/media.png">
										<p>Plus d’infos sur <a href="http://www.insa-euromediterranee.org" target="_blank">www.insa-euromediterranee.org</a></p>
									</div>

									<div class="tab-pane fade" id="home9" role="tabpanel">
										<p>Filières d’Ingénieur accréditées par l’Etat Marocain (CNACES). </p>

										<p>Diplômes d’Ingénieur reconnus par l’Etat Français (CTI)</p>
									</div>
									<div class="tab-pane fade" id="profile" role="tabpanel">
										<p>L’ingénieur-e INSA peut travailler en France et à l’étranger dans l’industrie, les PME-PMI, les bureaux d’études, les laboratoires de R&D, les cabinets conseil et dans tous les secteurs des sciences de l’ingénieur. L’INSA Euro-Méditerranée privilégie 4 secteurs économiques liés à la formation:</p>
										<img src="images/media1.png">

									</div>
									<div class="tab-pane fade formationinfo" id="frais" role="tabpanel">
										<div class="col-xs-12">

											<strong>Niveau d’admission</strong>
											<p>En 1ère année à l’INSA Euro-Méditerranée à Fès (bacheliers scientifiques)</p>
<p>En 2ème année à l’INSA Euro-Méditerranée à Fès (titulaires d’un bac+1 scientifique)</p>
<p>En 3ème année, à l’INSA Euro-Méditerranée en mobilité de 12 mois dans un INSA de France ou dans un établissement partenaire en Europe (titulaires d’un bac+2 scientifique) + titulaire  du DELF, DALF ou TCF </p>
<p>En 4ème année (titulaires au minimum d’une première année de master)</p>

											<strong>Procédure de candidature</strong>
<ul class="rounded">
<li>Admission en 1ère année</li>
</ul>
<ul class="dashed">
	 <li>BAC S du système français : du 20 janvier au 20 mars sur APB, www.admission-postbac.fr</li>
   <li>Autre BAC scientifique: à partir du 20 janvier sur <a href="http://admission.groupe-insa.fr" target="_blank">http://admission.groupe-insa.fr</a></li>
</ul>

<ul class="rounded">
<li>Admission en 2ème année: À partir du 20 janvier sur <a href="http://admission.groupe-insa.fr" target="_blank">http://admission.groupe-insa.fr</a></li>
<li>Admission en 3ème année: À partir du 20 janvier sur <a href="http://admission.groupe-insa.fr" target="_blank">http://admission.groupe-insa.fr</a></li>
<li>Admission en 4ème année Par mail: admission-insa@ueuromed.org</li>
</ul>



<ul>Frais de candidature : 95 €</ul>











											<strong>Frais scolaires</strong>
											<p>Pour les années 1, 2 et 3 : 57 000 dirhams / an (*)</p>
											<p>Pour les années 4 et 5 : 72 000 dirhams / an (*)</p>
											<p>(*) tarif pouvant être soumis à modification.</p>

											<strong>Aide financière</strong>
										 <ul class="rounded">
							<li>Bourses de mérite (Critères sociaux + critères du dossier académique) </li>
							<li>Bourses d’excellence (Critères du dossier académique)  </li>
							<p> Ces bourses peuvent couvrir 20%, 50% ou 100% des frais de scolarité. </p>
							</ul>




											<strong>Logement</strong>
											<p>Des logements pour les étudiants sont disponibles sur le campus de l’université UEMF.</p>
										</div>
										<div class="row tool-tip yellow">
											<div class="col-xs-12">
												<div class="row">
													<div class="col-sm-12 no-padding-left"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="page-section white" style="padding-top: 50px; padding-bottom: 50px">
			<section class="contact-us yellow">
				<div class="container">
					<div class="owl-theme cetraleslide" id="owl-demo">

						<div class="item"><img alt="Owl Image" src="images/insa/Insa2.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/insa/Insa3.jpg"></div>
								<div class="item"><img alt="Owl Image" src="images/insa/Insa5.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/insa/Insa4.jpg"></div>

						<div class="item"><img alt="Owl Image" src="images/insa/Insa maquette.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/insa/Insa maquette1.jpg"></div>
					</div>
				</div>
			</section>
		</div>
		<div class="page-section short" id="s6" style="padding: 12px 0;">
			<section class="contact-us yellow contact-us2">
				<div class="container">
					<h1 class="short">NOUS CONTACTER</h1>
					<div class="row">
						<div class="two-columns">
							<div class="col-sm-4 col-xs-12 padding">
								<div class="field-column yellow border contactb">
									<strong class="real-estate">ADRESSE</strong>
									<p>Adresse : INSA Euro-Méditerranée Université Euro-Med de Fès Route de Meknès (RN6, face à Auto-Hall), BP 51 - Fès 30 070 Fès, Maroc </p>
								</div>
							</div>
							<div class="col-sm-8 col-xs-12 padding">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.950801686647!2d-5.064473!3d34.0451331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda0200167cdd43d%3A0x89b0ef8e8740d496!2suniversit%C3%A9+Euro-M%C3%A9diterran%C3%A9enne+de+F%C3%A8s+UEMF!5e0!3m2!1sen!2s!4v1504685271823" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="page-section short2">
			<section class="contact-us yellow">
				<div class="container">
					<div class="row">
						<div class="two-columns">
							<h1 class="short">Vous avez besoin de plus d'informations ?</h1>
							<div class="col-sm-6 col-xs-12 padding ginfo">
								<span class="titleinfo"></span> <span class="tel cinfo"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:+212 538 903 224">+212 (0) 5 38 90 32 24</a></span> <span class="mail cinfo"></span> <span class="website cinfo"><i aria-hidden="true" class="fa fa-globe"></i> <a href="http://www.insa-euromediterranee.org" target="_blank">www.insa-euromediterranee.org</a></span>
								<ul id="social-media-etab">
									<li>
										<a href="https://www.facebook.com/INSAEuroMediterranee" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a href="https://twitter.com/INSAEuroMed" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a>
									</li>
								</ul>
							</div>
            <div class="col-sm-6 col-xs-12 padding">
                            <form action="mail.php" class="contact-form" method="POST">
                                <input placeholder="Nom & Prénom*" type="text" name="full_name"> <input placeholder="Email*" type="email" name="email"> <input placeholder="Sujet" type="text" name="sujet">
                                <textarea placeholder="Message" name="message"></textarea>
                                <div class="holder yellow">
                                    <button type="submit">Envoyer</button> <span><em>*</em>Champs obligatoires</span>
                                </div>
                                <input type="hidden" name="to" value="direction@insa.ueuromed.org" />
                            </form>
                        </div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div><?php include 'includes/footer.php'?>
	<script src="js/jquery.min.js">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->

	<script src="js/owl.carousel.js">
	</script>
	<script type="text/javascript">
	              $(document).ready(function() {

	                  $("#owl-demo").owlCarousel({
	                        autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

	                      responsive: {
	                          0: {
	                              items: 1,
	                              nav: false
	                          },
	                          600: {
	                              items: 2,
	                              nav: false
	                          },
	                          1000: {
	                              items: 3,
	                              nav: false,

	                          }
	                      }

	                  });

	              });
	</script>
	<script src="js/bootstrap.min.js">
	</script>
	<script src="js/switcher.js">
	</script>
	<script src="js/custom.js">
	</script>
</body>
</html>
