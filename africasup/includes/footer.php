<footer id="footer-2">
    <div class="container">
        <div class="footer-column">
            <div class="column_1 colfooter">
                <div class="logo"><a href="http://africasup.org/"><img src="images/logo46.png" width="174" height="19" alt="descipline"></a>
                </div>


            </div>

            <div class="column-2 colfooter">
                <span class="heading">Nos établissements</span>
                <ul class="links">
                    <li><a href="centrale-casa.php">Ecole Centrale Casablanca </a></li>
                    <li><a href="emlyon.php">emlyon business school campus Casablanca</a></li>
                    <li><a href="ESSEC.php">ESSEC Afrique-Atlantique</a></li>
                    <li><a href="INSA.php">INSA Euro-Méditerranée</a></li>
                    <li><a href="EMINES.php">EMINES - School of Industrial Management</a></li>
                </ul>
            </div>
            <div class="column_4 ">
                <div class="footer-logo-right footer-logo-right1">
                    <a href="https://ma.ambafrance.org/"><img src="images/logoaf.jpg" alt=""></a>
                <br/>
                <br/>
                <br/>
                <a href="https://if-maroc.org/"><img src="images/if1.jpg" alt=""></a>
                </div>

                <div class="footer-logo-right footer-logo-right2">
                    <a href="#"><img src="images/f2.jpg" alt=""></a>
                <br/>
                <br/>
                <br/>
                <a href="#"><img src="images/f1.png" alt=""></a>
                </div>
            </div>

        </div>
    </div>
</footer>
<div class="bottom-footer inner">
    <span>Copyright &copy; 2017. Product by <strong>AdelphaTech Inc</strong></span>
</div><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->