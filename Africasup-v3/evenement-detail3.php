<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- title of the page -->
<title>Événement Detail</title>

<!-- Bootstrap style sheet -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- css style sheet -->
<link rel="stylesheet" href="css/style.css">
<link id="style" rel="stylesheet" href="css/custom.css">

<link id="colors" rel="stylesheet" href="css/color-blue.css">
<link rel="stylesheet" href="css/switcher.css">
        
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"> 

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

 
<!-- main wrapper of the page -->
<div id="wrapper"> 
	<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png" 
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png" 
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>

			</div>
		</div>

	<?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img src="images/ce.png" width="1920" height="157" alt="image">
			<div class="banner-text">
				<h2>TOURNÉE DE L'OUEST AFRICAIN : SALON « FORMATIONS ET 1ER EMPLOI » AU DAKAR.
</h2>	</div>
		</div>
				<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>TOURNÉE DE L'OUEST AFRICAIN : SALON « FORMATIONS ET 1ER EMPLOI » AU DAKAR.</li>
			</ul>
		</div>
		
	
	<div id="main">
		<div class="container">
			<div class="col-xs-12">
				<div class="row">
					<section class="latest-news inner yellow">
						<div class="row">
							<div class="col-xs-12">
								<div class="img-holder">
									<img src="images/https _cdn.evbuc.com_images_36045690_226408793643_1_original.jpg" width="370" height="300" alt="image">
									<span class="date">
										17 <span style="font-size:20px;">au</span> 18 <span style="font-size:20px;">octobre</span> 2017
									</span>
								</div>
								<div class="news-text">
									<span class="title">Tournée de l'Ouest Africain :  Salon « formations et 1er emploi » au Dakar.</span>
									<p> L’INSA euro-med et l’emlyon Casablanca seront présents ensembles à Dakar sur le salon « formations et 1er emploi » du 17 au 18 octobre. Ce Salon offre aux jeunes en phase d’insertion professionnelle une opportunité unique de rencontrer des établissements d’enseignement supérieur français et africains, des conseillers d’orientation, des entreprises, des spécialistes de l’emploi et du développement personnel. Cet évènement permettra à nos établissements d'enseignement supérieur de nouer de futurs partenariats.
 								</p>
 								<a href="http://www.senegal.campusfrance.org/actualite/salon-formations-et-1er-emploi">http://www.senegal.campusfrance.org/actualite/salon-formations-et-1er-emploi</a>
									
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>

	<?php include 'includes/footer.php'?>
			
		</div>

 

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/switcher.js"></script>
	<script src="js/custom.js"></script>		

	
	</body>
</html>
