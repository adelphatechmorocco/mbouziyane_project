
<header id="header2" class="header4 header-yellow">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
  
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#" style="pointer-events: none">Africa Sup</a>
                        <ul class="drop-down d-7">
                            <li><a href="http://africasup.org#s1" data-value="s1" id="qui">Qui sommes-nous?</a></li>
                            <li><a href="http://africasup.org#s2" data-value="s2">Notre vision</a></li>
                            <li><a href="http://africasup.org#s3" data-value="s3">Mot d’accueil</a></li>

                        </ul>

                    </li>
                    <li><a href="#" style="pointer-events: none">Nos établissements</a>
                        <ul class="drop-down d-7">
                            <li><a href="centrale-casa.php">Ecole Centrale Casablanca</a></li>
                            <li><a href="EMINES.php">EMINES - School of Industrial Management</a></li>
                            <li><a href="emlyon.php">EmLyon Business School, Campus Casablanca </a></li>
                            <li><a href="ESSEC.php">ESSEC Afrique-Atlantique</a></li>
                            <li><a href="INSA.php">INSA Euro-Méditerranée </a></li>

                        </ul>
                    </li>
                    <li><a href="alumni.php">Alumni</a></li>
                    <li><a href="evenement.php">Événements & Actualités</a></li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="env tel home layer"><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="contacter.php">Nous contacter</a></div>

</header>