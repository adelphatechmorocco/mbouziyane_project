<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>Alumni</title><!-- Bootstrap style sheet -->
	<link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/custom.css" id="style" rel="stylesheet">
	<link href="css/color-blue.css" id="colors" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/switcher.css" rel="stylesheet">
	<link href="css/layout.css" rel="stylesheet">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- main wrapper of the page -->
	<div id="wrapper">
		<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
			</div>
		</div>
		<?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img alt="image" height="157" src="images/ce.png" width="1920">
			<div class="banner-text">
				<h1>Alumni</h1>

			</div>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>Alumni</li>
			</ul>
		</div>

		<section class="page-section short ">
		<div class="container">
				    <div class="tool-tip yellow">

							<div class="col-sm-12 col-md-6">
								<img src="images/Dipklomé.jpg" alt="">
							</div>



			<div class="col-sm-12 col-md-6 heading-short alumtext">
			<h2 class="short text-left custom-title custom-title-vert">Un réseau Alumni, qu’est-ce que c’est ? </br><span> C’est une communauté de diplômés du même établissement. </span></h2>
				<h2 class="short text-left custom-title custom-title-vert">Pourquoi intégrer ces réseaux ? </br><span class="text-center">Les communautés d’alumni ont pour vocation de faciliter l'échange des savoirs et le partage d'expériences professionnelles entre anciens étudiants diplômés des écoles.</span>	</h2>
			</div>

			</div>




			<div class="tool-tip yellow col-xs-12 alumtext">
				<div class="col-xs-12">
					<p style="float: left;">Les réseaux de diplômés ont pour <strong class="objtitle">objectifs </strong> :</p>
<ul class="list-item-1">

	<li class="text-justify"><b style="color: #208D86;">L’entraide entre élèves et anciens diplômés :</b> aider des élèves à se constituer un capital relationnel précieux et à trouver un emploi en diffusant notamment des offres d'emploi via la plateforme du réseau,</li>
	<li class="text-justify"><b style="color: #208D86;">Renforcer les liens :</b> entre alumni et les entreprises,</li>
	<li class="text-justify"><b style="color: #208D86;">Garder un lien :</b> avec l’Ecole en se tenant informé des dernières actualités de l’établissement et de la vie sociale de l’association.

	</li>
</ul>



				</div>

				<div class="col-xs-12">
					</br>
					 <p  class="text-justify">Les réseaux de diplômés des écoles membres d’Africa Sup sont pour la majorité rattachés aux associations de leur groupe qui fédèrent les alumni en France et à travers le monde (clubs d’affaires, correspondants pays, rencontres internationales, afterwork…). Ainsi, quand un jeune diplômé veut s’installer à l’étranger, la communauté d’alumni peut le mettre en relation avec des anciens qui pourront, entre autre, le guider dans ses recherches d’emploi.
					 </p>

				</div>

			</div>
		</div>
	</section>


<div class="page-section white">
		<div class="container">
			<div>
				<section class="services services-alumni team_2">
					<div class="col-xs-12 heading-short">
					<h1 class="short">Les Alumni</h1>
			    	</div>

					<div class="rowser">
						<div class="col-sm-6 col-xs-12">
							<div class="services-column">
								<strong>ESSEC Alumni</strong>

								<p class="text-justify"><a href="http://www.alumni.essec.edu/fr/" class="logo-etab" target="_blank"><img src="images/head-logo.png" alt="" ></a> ESSEC Alumni supervise des chapters dans plus de 60 pays sur les 5 continents. Ils permettent aux milliers de diplômés et étudiants disséminés dans le monde de rester en lien avec l’association et l’école.

								</p>
								<p class="text-justify">Le chapter au Maroc comprend plus de 100 alumni décideurs dans les secteurs clés de l’économie marocaine, dirigeants, consultants, financiers, business developpers, développeur de talent.
								</p>
								<a class="more morealu" href="http://www.alumni.essec.edu/fr" target="_blank">Site internet</a>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12">
							<div class="services-column">
								<strong> Réseau des centraliens </strong>
								<p class="text-justify"><a href="http://www.centraliens.ma" class="logo-etab"  target="_blank"><img src="images/logo_aecp.jpg" alt=""></a>Au Maroc, l’Association des Centraliens regroupe plus de 400 diplômés, dont un grand nombre assument de hautes responsabilités dans tous les secteurs d’activité : Industrie, Service, Finance ou Fonction publique.
								</p>
								<p class="text-justify">L’Association des Centraliens de Paris se rapproche des associations d’alumni des autres Écoles Centrale et de Supélec. Les diplômés de Centrale Casablanca sont, de fait, membres de l’Association.</p>
								<a class="more morealu" href="http://www.centraliens.ma" target="_blank">Site internet</a>
							</div>
						</div>
					</div>

					<div class="rowser">
						<div class="col-sm-6 col-xs-12">
							<div class="services-column">
								<strong> INSA Alumni </strong>
								<p class="text-justify"><a href="http://www.insa-alumni.org/fr" class="logo-etab" target="_blank"><img src="images/logomm.png" alt="" ></a>L’association des INSA alumni au Maroc accompagne avec dynamisme le développement de l’institut. INSA Alumni  est « un réseau en action partout dans le monde ».
								</p>
								<a class="autre more morealu" href="http://www.insa-alumni.org/fr/" target="_blank">Site internet</a>
							</div>
						</div>



						<div class="col-sm-6 col-xs-12">
							<div class="services-column">
								<strong> Emlyon forever </strong>
								<p class="text-justify"><a href="http://www.casablanca.em-lyon.com/reseau-des-diplomes" class="logo-etab"  target="_blank"><img src="images/logo-emlyon.png" alt=""></a> Avec emlyon business school forever, chaque étudiant devient membre du réseau de diplômés dès son premier jour à l’école... et pour toujours. Un atout pour construire son réseau et bénéficier d’une gamme de services avant même l’obtention de son diplôme.
								</p>
								<a class="more morealu" href="http://www.casablanca.em-lyon.com/reseau-des-diplomes " target="_blank">Site internet</a>
							</div>
						</div>
					</div>

				</section>
			</div>
		</div>
	</div>



		<br/>
		<br/>




<section class="page-section short short3">
			<div class="container">
				<div class="col-xs-12 heading-short">
					<h1 class="short">France Alumni Maroc</h1>
				</div>
				<div class="tool-tip yellow">
					<div class="col-sm-12">

						<p class="text-justify"><a href="https://www.francealumni.fr/fr/poste/maroc/" target="_blank"><img src="images/LOGO-FA-HD-PNG-pour-le-web (002).png" alt="" class="img-responsive" style="width: 433px; float: left"></a>En intégrant un des établissements de la plateforme Africa Sup, vous aurez l’opportunité d’effectuer un séjour hors Maroc, soit au sein du réseau de cette école en France ou dans de leurs établissements partenaires.













						</p>
						<p class="text-justify"Tout étudiant ayant étudier dans un établissement de l’enseignement supérieur français est éligible pour s’inscrire sur France Alumni Maroc, même si l’établissement en question est établi en dehors de l’hexagone.
						</p>

						<p class="text-justify">La plateforme France Alumni est un outil numérique innovant rassemblant les étudiants et diplômés du monde entier formés dans l’un des établissements de l’enseignement supérieur français. Chaque année, 100 000 étudiants internationaux sortent diplômés du système éducatif français. Pendant et après leurs études supérieures, France alumni les informe, leur permet de rester en réseau.

						<p class="text-justify">Pour procurer à tous la bonne information au moment adéquat, des ressources liées aux filières éducatives en France, des espaces réservés aux grandes entreprises françaises, des offres d’emploi et des newsletters thématiques sont mis à jour régulièrement. Le site propose aussi dans sa rubrique magazine des actualités sur la vie culturelle, économique et entrepreneuriale française. Il relaie les événements les plus intéressants pour les alumni.

						</p>
						<p class="text-justify">France alumni est la plateforme de référence pour les anciens étudiants internationaux qui souhaitent valoriser leur expérience en s’inscrivant au sein d’un vaste réseau français.</p>
					</div>
					</br>
							<a class="more morealu" href="https://www.francealumni.fr/fr/poste/maroc/" target="_blank">Site internet</a>


				</div>
			</div>
		</section>


		<div class="page-section" style="padding-top: 50px; padding-bottom: 50px">
			<section class="contact-us yellow">
				<div class="container">
					<div class="owl-theme cetraleslide" id="owl-demo">
						<div class="item"><img alt="Owl Image" src="images/people11.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/people21.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/people31.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/Logo-1.png"></div>
						<div class="item"><img alt="Owl Image" src="images/people41.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/logo-2.png"></div>


					</div>
				</div>
			</section>
		</div>


	</div>
<?php include 'includes/footer.php'?>
	<script src="js/jquery.min.js">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="js/owl.carousel.js">
	</script>
	<script type="text/javascript">
	       $(document).ready(function() {

	 $("#owl-demo").owlCarousel({
	     autoPlay: 3000, //Set AutoPlay to 3 seconds
	         responsiveClass:true,
	         loop: true,
	         slideSpeed : 2000,

	   responsive:{
	       0:{
	           items:1,
	           nav:false
	       },
	       600:{
	           items:2,
	           nav:false
	       },
	       1000:{
	           items:3,
	           nav:false,

	       }
	   }




	 });

	});


	</script>


	<script src="js/bootstrap.min.js">
	</script>
	<script src="js/switcher.js">
	</script>
	<script src="js/custom.js">
	</script>
</body>
</html>
