<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>EmLyon Business School Campus de Casablanca</title><!-- Bootstrap style sheet -->
	<link href="css/bootstrap.min.css" rel="stylesheet"><!-- css style sheet -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/custom.css" id="style" rel="stylesheet">
	<link href="css/color-blue.css" id="colors" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/switcher.css" rel="stylesheet">
	<link href="css/layout.css" rel="stylesheet">
	<link rel="stylesheet" href="css/responsive.css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i%2cOxygen:300,400,700" rel="stylesheet"><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="central-casa">
	<!-- main wrapper of the page -->
	<div id="wrapper">
		<div class="top-bar4">
			<div class="container">
				 <div class="logo"><a href="http://africasup.org/"><img src="images/logo45.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logomobile"><a href="http://africasup.org/"><img src="images/logo44.png"
                                                                   alt="descipline" class="img-responsive"></a></div>
                                                                   <div class="logotitle">Le premier regroupement de Grandes Écoles soutenues par la France au Maroc</div>
			</div>
		</div><?php include 'includes/header.php'?>
		<div class="banner-2 yellow">
			<img alt="image" height="157"  style="height: 260px;" src="images/ce.png" width="1920">
			<div class="banner-text">
				<h1 style="text-transform: lowercase; font-family: 'Alegreya Sans', sans-serif; ">emLyon business school campus de <span style="text-transform: capitalize;"">casablanca</span> </h1>
				<h3>early makers</h3>
			</div>
		</div>
		<div class="container">
			<ul class="breadcrumb">
				<li>
					<a href="http://africasup.org/">Accueil</a>
				</li>
				<li>EmLyon Business School Campus de Casablanca</li>
			</ul>
		</div>
		<section class="page-section white short" style="padding: 13px 0;">
			<div class="container">
				<div class="row tool-tip yellow">
					<div class="col-sm-4"><img src="images/emlogo.png"></div>
					<div class="col-sm-8">
						<p class="text-justify">Fondée en 1872, emlyon business school est l'une des plus anciennes écoles de commerce en Europe. L'histoire d'emlyon business school a toujours été profondément marquée par l'entrepreneuriat, l'innovation et la volonté de réussir. Ces trois facteurs expliquent pourquoi l'Ecole est devenue aujourd'hui un phare de l'éducation entrepreneuriale en France, en Europe et dans le monde.Aujourd’hui, nous œuvrons à révéler, préparer et faire grandir des talents aptes à saisir les opportunités, construire le futur et suggérer des réponses innovantes aux défis du monde de demain, avec une capacité à passer à l’action (« doer, maker ») et à se confronter au réel : c’est tout le sens de notre nouvelle signature « Early Makers ».</p>

					</div>
					<div class="col-md-12"><p>Le Campus Casablanca d’emlyon business school est véritablement international : outre le fait d’accueillir des étudiants venant de différents pays d’Afrique, il accueille chaque année des étudiants des autres campus d’emlyon, des étudiants en échange académique en provenance de nos partenaires internationaux d’Europe, d’Amérique ou d’Asie ou dans aussi dans le cadre des summer sessions.</p>
						<p>Après seulement un an, le campus compte déjà plus de 10 nationalités différentes. Des étudiants provenant d’autres pays seront amenés à rejoindre le campus avec la mise en place du Global BBA full English track dès la rentrée 2017.</p></div>
				</div>
			</div>
		</section>
		<div class="page-section short">
			<div class="container">
				<div class="row">
					<section class="services-tab short col-md-12">
						<div>
							<div class="tab_2 second yellow">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active" role="presentation">
										<a aria-controls="home3" data-toggle="tab" href="#home3" role="tab"><img alt="" class="center-block" src="images/business-people-meeting.png" style="display: block">Formation Proposée</a>
									</li>
									<li role="presentation">
										<a aria-controls="home" data-toggle="tab" href="#home9" role="tab"><img alt="" class="center-block" src="images/diploma.png" style="display: block">Reconnaissance du diplôme</a>
									</li>
									<li role="presentation">
										<a aria-controls="profile" data-toggle="tab" href="#profile" role="tab"><img alt="" class="center-block" src="images/icon-suit.png" style="display: block"> Débouchés professionnels</a>
									</li>
									<li role="presentation">
										<a aria-controls="frais" data-toggle="tab" href="#frais" role="tab"><img class="center-block" src="images/graduate-showing-his-diploma.png" style="display: block">Intégrer notre établissement</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="#" id="linkmsg"><img alt="" class="center-block" src="images/email.png" style="display: block"> Plus d'informations</a>
									</li>
									<li class="tablink" role="presentation">
										<a data-value="s6" href="http://casablanca.em-lyon.com/" target="_blank" id="linkmsg"><img alt="" class="center-block" src="images/www.png" style="display: block"> Site de l’établissement </a>
									</li>
								</ul><!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane fade in active" id="home3" role="tabpanel">
										<p><strong>Formations à plein temps</strong></p>
										<ul>
											<li><strong>Global BBA</strong> :<a href="http://casablanca.em-lyon.com/global-bba-en-4-ans/">http://casablanca.em-lyon.com/global-bba-en-4-ans/</a> </li>
											<li><strong>Global BBA full English track</strong>: <a href="http://casablanca.em-lyon.com/global-bba-en-4-ans/">http://casablanca.em-lyon.com/global-bba-en-4-ans/</a></li>
											<li><strong>Global MS Track</strong> :<a href="http://casablanca.em-lyon.com/global-ms-track-mastere/">http://casablanca.em-lyon.com/global-ms-track-mastere/</a></li>
										</ul>
										<p><strong>Formations à temps partiel</strong></p>
										<ul>
											<li><strong>Executive MBA</strong> :<a href="http://casablanca.em-lyon.com/executive-mba/">http://casablanca.em-lyon.com/executive-mba/</a></li>
											<li><strong>Mastère Spécialisé® Entreprendre</strong> :<a href="http://casablanca.em-lyon.com/masteres-specialises/m-s-entreprendre/">http://casablanca.em-lyon.com/masteres-specialises/m-s-entreprendre/</a></li>
											<li><strong>Mastère Spécialisé® Ingénierie Financière</strong> :<a href="http://casablanca.em-lyon.com/masteres-specialises/m-s-ingenierie-financiere/">http://casablanca.em-lyon.com/masteres-specialises/m-s-ingenierie-financiere/</a></li>
											<li><strong>Mastère Spécialisé® Marketing et Management des Services</strong> :<a href="http://casablanca.em-lyon.com/masteres-specialises/m-s-marketing-management-services/">http://casablanca.em-lyon.com/masteres-specialises/m-s-marketing-management-services/</a>
											<li><strong>Programme Général de Management</strong> :<a href="http://casablanca.em-lyon.com/programme-general-de-management/">http://casablanca.em-lyon.com/programme-general-de-management/</a></li>
											<li><strong><a href="http://casablanca.em-lyon.com/parcours-certifiants/">Parcours certifiants</a> et <a href="http://casablanca.em-lyon.com/formations-courtes/">modules courts </a></strong></li>
										</ul>
										<p><strong>Formation continue sur-mesure</strong></p>

										<!--<p>Summer session <a href="http://casablanca.em-lyon.com/summer-programme/" target="_blank">www.casablanca.em-lyon.com/summer-programme/</a></p>-->
									</div>
									<div class="tab-pane fade" id="home9" role="tabpanel">
										<p>Triple accréditation depuis 2005, positionnant l'école parmi 1% de business schools dans le monde dotées de la "triple couronne" :  AACSB, Equis, Amba</p>
										<div class="col-sm-2 recon"><img src="images/r1.png"></div>
										<div class="col-sm-2 recon"><img src="images/r2.png"></div>
										<div class="col-sm-2 recon"><img src="images/r3.png"></div>
										<div class="col-sm-2 recon"><img src="images/r4.png"></div>
										<div class="col-sm-2 recon"><img src="images/r5.jpg"></div>
									</div>
									<div class="tab-pane fade" id="profile" role="tabpanel">
							<p> <strong>Les débouchés couvrent les principaux secteurs :</strong></p>
										<p>Gestion et finance, entrepreneuriat, affaires internationales, développement, marketing avec des fonctions telles que chargé d’affaires internationales, analyste financier, auditeur externe, business manager, ingénieur d’affaires, responsable de zone export, chargé d’études marketing, acheteur, responsable qualité...</p>

									</div>
					<div class="tab-pane fade" id="frais" role="tabpanel">
			<div class="col-xs-12">
				<strong>Niveau d’admission</strong>
				<p>
					Admission tous niveaux de Bac à Bac+5 selon le programme souhaité.
				</p><strong>Procédure de candidature</strong><br>

				<ul class="rounded">
					<li>

							<strong>Pour la formation initiale:</strong>

					</li>
				</ul>
				<ul class="dashed">
					<li>Dossier de candidature
					</li>
					<li>Concours d'admission
					</li>
					<li>Entretien
					</li>
				</ul>
				<ul class="rounded">
					<li><strong>Pour la formation continue:</strong></li>
				</ul>
				<ul class="dashed">
					<li>Dossier de candidature
					</li>
					<li>Entretien
					</li>
				</ul>

				<strong>Frais scolaires</strong>
				<br>

				<ul class="rounded">
					<li>
							Pour la formation initiale : Entre 105 000 et 145 000 MAD
					</li>
					<li>
							Pour la formation continue: : Entre 11 400 et 295 000 MAD
					</li>
				</ul>
				<strong>Aide financière</strong>
				<p>
					Il est possible d'accéder à des prêts bancaires négociés auprès des partenaires d’emlyon business school Campus Casablanca, remboursables après une période de franchise et à taux préférentiel.
				</p>
				<p>
					Il est également possible de bénéficier de <strong>bourses d’études</strong> emlyon business school campus Casablanca selon des critères sociaux et d’excellence académique.
				</p><strong>Logement</strong>
				<br>
				<ul class="rounded">
					<li>Accompagnement des étudiants en cas de besoin pour trouver un logement à proximité du campus d'emlyon business school.
					</li>
					<li>Accord de partenariat signé avec les résidences universitaires à Casablanca.
					</li>
				</ul>
			</div>
			<div class="row tool-tip yellow">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-sm-12 no-padding-left"></div>
					</div>
				</div>
			</div>
		</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="page-section white" style="padding-top: 50px; padding-bottom: 50px">
			<section class="contact-us yellow">
				<div class="container">
					<div class="owl-theme cetraleslide" id="owl-demo">
						<div class="item"><img alt="Owl Image" src="images/emlyon1.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/emlyon2.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/emlyon3.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/emlyon4.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/emlyon5.jpg"></div>
						<div class="item"><img alt="Owl Image" src="images/emlyon6.jpg"></div>

					</div>
				</div>
			</section>
		</div>
		<div class="page-section short" id="s6" style="padding: 12px 0;">
			<section class="contact-us yellow contact-us2">
				<div class="container">
					<h1 class="short">NOUS CONTACTER</h1>
					<div class="row">
						<div class="two-columns">
							<div class="col-sm-4 col-xs-12 padding">
								<div class="field-column yellow border contactb">
									<strong class="real-estate">ADRESSE</strong>
									<p>Adresse : Marina de Casablanca, Tour Crystal 1, 6ème étage Casablanca</p>
								</div>
							</div>
							<div class="col-sm-8 col-xs-12 padding">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3322.872344151855!2d-7.626929735178371!3d33.6086165307289!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda7d262fcdc3af9%3A0xf59a8bafc00ee234!2sEMLYON+Business+School!5e0!3m2!1sen!2s!4v1505303114198" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="page-section short2">
			<section class="contact-us yellow">
				<div class="container">
					<div class="row">
						<div class="two-columns">
							<h1 class="short">Vous avez besoin de plus d'informations ?</h1>
							<div class="col-sm-6 col-xs-12 padding ginfo">
								<span class="titleinfo"></span> <span class="tel cinfo"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:+212522493500">+212 (0) 5 22 49 35 00</a></span> <span class="mail cinfo"></span> <span class="website cinfo"><i aria-hidden="true" class="fa fa-globe"></i> <a href="http://casablanca.em-lyon.com/" target="_blank">www.casablanca.em-lyon.com</a></span>
								<ul id="social-media-etab">
									<li>
										<a href="https://www.facebook.com/emlyonCasa/" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a href="https://twitter.com/emlyon_casa" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a>
									</li>
									<li>
										<a href="https://www.linkedin.com/edu/school?id=12324&trk=tyah&trkInfo=clickedVertical%3Aschool%2CclickedEntityId%3A12324%2Cidx%3A4-1-11%2CtarId%3A1481890872873%2Ctas%3Aemlyon%20" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a>
									</li>
									<li>
										<a href="https://plus.google.com/+emlyon" target="_blank"><i aria-hidden="true" class="fa fa-google"></i></a>
									</li>
								</ul>
							</div>
						<div class="col-sm-6 col-xs-12 padding">
                            <form action="mail.php" class="contact-form" method="POST">
                                <input placeholder="Nom & Prénom*" type="text" name="full_name"> <input placeholder="Email*" type="email" name="email"> <input placeholder="Sujet" type="text" name="sujet">
                                <textarea placeholder="Message" name="message"></textarea>
                                <div class="holder yellow">
                                    <button type="submit">Envoyer</button> <span><em>*</em>Champs obligatoires</span>
                                </div>
                                <input type="hidden" name="to" value="contact.casablanca@em-lyon.com" />
                            </form>
                        </div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div><?php include 'includes/footer.php'?>
	<script src="js/jquery.min.js">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->

	<script src="js/owl.carousel.js">
	</script>
	<script type="text/javascript">
	              $(document).ready(function() {

	                  $("#owl-demo").owlCarousel({
	                       autoPlay: 3000, //Set AutoPlay to 3 seconds
            responsiveClass: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true,

	                      responsive: {
	                          0: {
	                              items: 1,
	                              nav: false
	                          },
	                          600: {
	                              items: 2,
	                              nav: false
	                          },
	                          1000: {
	                              items: 3,
	                              nav: false,

	                          }
	                      }

	                  });

	              });
	</script>
	<script src="js/bootstrap.min.js">
	</script>
	<script src="js/switcher.js">
	</script>
	<script src="js/custom.js">
	</script>
</body>
</html>
