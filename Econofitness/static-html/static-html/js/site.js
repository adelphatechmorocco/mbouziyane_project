$(document).ready(function(){

function navHover(){
    $('.has-dropdown').hover(function(){
        $(this).find('.nav-dropdown').stop().slideToggle('fast');
    });
}

function toggleNavSearch(){
    $('.nav-top__links--search a').click(function(){
        $(this).parent().toggleClass('active');
        $('.nav-search').focus();
    });
}

function scrolledNav(){
     $(document).scroll(function() {
        if ( $(document).scrollTop() >= 200 ) {
            $('.nav-scrolled').addClass('active');
        } else{
            $('.nav-scrolled').removeClass('active');
        }
    });
}
function collapseComparisonTable(){
    $('.membership-options-table--compare').click(function(){
        $(this).toggleClass('active');
        $('.membership-options-table tbody:not(.membership-options-table--header)').toggle();
    });
}
function initTestimonialCarousel(){
    $('.testimonial-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ["<img src='img/arrow-left.png'>","<img src='img/arrow-right.png'>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
    });
}

function heroLocationSelect(){
    $('.hero-location-finder--dropdown span.dropdown').click(function(){
        $('.hero-location-finder--list').slideToggle('fast');
        $(this).toggleClass('active');
    });
}

navHover();
toggleNavSearch();
scrolledNav();
collapseComparisonTable();
initTestimonialCarousel();
heroLocationSelect();

});