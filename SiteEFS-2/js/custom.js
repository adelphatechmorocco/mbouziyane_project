   $(document).ready(function() {
    $('.slider').owlCarousel({
      items: 4,
      autoplay: true,
      navigation: true,
      itemsDesktop: [1200, 3], //5 items between 1000px and 901px
      itemsDesktopSmall: [900, 2], // betweem 900px and 601px
      itemsTablet: [650, 1], //2 items between 600 and 0;
    });
    $('header .open-menu i').on('click', function() {
      $('.desktop-menu').toggle('500');
      $(this).toggleClass('fa-times fa-bars');
    });
    /*SmallScreen();*/
    $(window).resize(function() {
      if ($(window).width() > 768) {
        $(".desktop-menu").attr('style', '');
      }
      /*SmallScreen();*/
    });
    $(".desktop-menu").click(function(e) {
      e.stopPropagation('slow');
    });
    /*  function SmallScreen(){
        $(document).click(function(){
          if($(window).width() < 768){
            if(!$(this).hasClass('desktop-menu'))
            {
               $(".desktop-menu").hide('slow');
            }
          }
         });
      }*/
    /**
     * Push right instantiation and action.
     */
    var pushRight = new Menu({
      wrapper: '#o-wrapper',
      type: 'push-right',
      menuOpenerClass: '.c-button',
      maskId: '#c-mask'
    });
    var pushRightBtn = document.querySelector('#c-button--push-right');
    pushRightBtn.addEventListener('click', function(e) {
      e.preventDefault;
      pushRight.open();
    });
    $('#header a[href*="#"]:not([href="#"]), footer a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - 110
          }, 1000);
          return false
        }
      }
    });
   });